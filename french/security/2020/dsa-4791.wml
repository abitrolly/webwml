#use wml::debian::translation-check translation="b35f3578f60fe18b9150a0924ba18826c4d6be4c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Ken Gaillot a découvert une vulnérabilité dans le gestionnaire de
ressources de grappe Pacemaker : si les ACL ont été configurées pour des
utilisateurs dans le groupe <q>haclient</q>, les restrictions d'ACL
pourraient être contournées au moyen d'une communication inter-processus
non restreinte, avec pour conséquence dans l'exécution de code arbitraire
dans toute la grappe avec les privilèges du superutilisateur.</p>

<p>Si l'option de grappe <q>>enable-acl</q> n'est pas activée, les membres du
groupe <q>haclient</q> peuvent modifier sans restriction la base
d'informations de grappe de Pacemaker, qui leur donne déjà ces capacités,
aussi, il n'y a pas d'exposition supplémentaire avec une telle
configuration.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 2.0.1-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pacemaker.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pacemaker, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pacemaker">\
https://security-tracker.debian.org/tracker/pacemaker</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4791.data"
# $Id: $
