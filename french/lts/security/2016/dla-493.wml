#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8312">CVE-2015-8312</a>
<p>Une erreur due à un décalage d'entier dans afs_pioctl.c dans OpenAFS,
version antérieure à 1.6.16, pourrait permettre à des utilisateurs locaux
de provoquer un déni de service (écrasement de mémoire et plantage du
système) à l'aide d'un pioctl avec une taille de tampon d'entrée de
4096 octets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2860">CVE-2016-2860</a>
<p>La fonction newEntry dans ptserver/ptprocs.c dans OpenAFS, version
antérieure à 1.6.17, permet à des utilisateurs distants authentifiés
à partir d'un domaine Kerberos étranger de contourner des restrictions
d'accès voulues et de créer des groupes arbitraires comme s'ils étaient
administrateurs en exploitant un mauvais traitement de l'identifiant du
créateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4536">CVE-2016-4536</a>
<p>Le client dans OpenAFS, version antérieure à 1.6.17, n'initialise pas
correctement les structures (1) AFSStoreStatus, (2) AFSStoreVolumeStatus,
(3) VldbListByAttributes et (4) ListAddrByAttributes, ce qui pourrait
permettre à des attaquants distants d'obtenir des informations sensibles de
la mémoire en exploitant l'accès au trafic d'appel RPC.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.6.1-3+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openafs.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-493.data"
# $Id: $
