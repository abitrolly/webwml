#use wml::debian::translation-check translation="cb938baac9814c55250bf1b4ab272f9c64e7d963" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités mineures ont été découvertes dans libvncserver,
une mise en œuvre de serveur et de client pour le protocole VNC.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20839">CVE-2019-20839</a>

<p>libvncclient/sockets.c dans LibVNCServer possédait un dépassement de tampon
à l'aide d'un nom de fichier de socket long.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14397">CVE-2020-14397</a>

<p>libvncserver/rfbregion.c possédait un déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14399">CVE-2020-14399</a>

<p>Les données alignées sur des octets étaient obtenues au travers de pointeurs
uint32_t dans libvncclient/rfbproto.c.</p>

<p>NOTE : ce problème a été sujet à controverses par des tierces parties. Il
n’existe pas de signalement de franchissement de niveau de confiance (<q>no
trust boundary crossed</q>).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14400">CVE-2020-14400</a>

<p>Les données alignées sur des octets étaient obtenues au travers de pointeurs
uint16_t dans libvncserver/translate.c.</p>

<p>NOTE : ce problème a été sujet à controverses par des tierces parties. Il
n’existe pas de chemin connu pour une exploitation ou un franchissement de
niveau de confiance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14401">CVE-2020-14401</a>

<p>libvncserver/scale.c possédait un dépassement d’entier dans pixel_value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14402">CVE-2020-14402</a>

<p>libvncserver/corre.c permettait un accès hors limites à l’aide d’encodages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14403">CVE-2020-14403</a>

<p>libvncserver/hextile.c permettait un accès hors limites à l’aide d’encodages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14404">CVE-2020-14404</a>

<p>libvncserver/rre.c permettait un accès hors limites à l’aide d’encodages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14405">CVE-2020-14405</a>

<p>libvncclient/rfbproto.c ne limitait pas la taille de TextChat.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.9.11+dfsg-1.3~deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvncserver.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libvncserver, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libvncserver">https://security-tracker.debian.org/tracker/libvncserver</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2347.data"
# $Id: $
