#use wml::debian::translation-check translation="374e58b76c6b97a44bdab4ad3791d022a1849896" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de pacemaker publiée dans la DLA-2447-1 causait une régression
quand la communication entre le moteur de grappe Corosync et pacemaker se
faisait. Un problème de permission empêchait des requêtes IPC entre nœuds de
grappes. Le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25654">CVE-2020-25654</a>
est annulé jusqu’à ce qu’une meilleure solution ait été trouvée.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.1.16-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pacemaker.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pacemaker, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/pacemaker">https://security-tracker.debian.org/tracker/pacemaker</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2447-2.data"
# $Id: $
