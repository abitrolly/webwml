#use wml::debian::translation-check translation="2d8aa8ce4c88e03582c09befc9ad03c6b190bfbf" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes en parcourant
différents binaires liés à tiff. Des images TIFF contrefaites permettent
à des attaques distantes de provoquer un déni de service ou, dans certains
cas, l’exécution de code arbitraire par une division par zéro, une écriture
hors limites, un dépassement d'entier et de tas.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3622">CVE-2016-3622</a>

<p>La fonction fpAcc dans tif_predict.c dans l'outil tiff2rgba dans
LibTIFF 4.0.6 et antérieur permet à des attaquants distants de provoquer
un déni de service (erreur de division par zéro) à l'aide d'une image TIFF
contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3623">CVE-2016-3623</a>

<p>L'outil rgb2ycbcr dans LibTIFF 4.0.6 et antérieur permet à des attaquants
distants de provoquer un déni de service (division par zéro) en fixant
le paramètre (1) v ou (2) h à 0. (Corrigé avec le
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3624">CVE-2016-3624</a>.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3624">CVE-2016-3624</a>

<p>La fonction cvtClump dans l'outil rgb2ycbcr dans LibTIFF 4.0.6 et
antérieur permet à des attaquants distants de provoquer un déni de service
(écriture hors limites) en fixant l'option <q>-v</q> à -1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3945">CVE-2016-3945</a>

<p>Plusieurs dépassements d'entier dans les fonctions (1) cvt_by_strip et (2)
cvt_by_tile dans l'outil tiff2rgba dans LibTIFF 4.0.6 et antérieur, lorsque
le mode -b est activé, permettent à des attaquants distants de provoquer un déni
de service (plantage) ou d'exécuter du code arbitraire à l'aide d'une image
TIFF contrefaite, ce qui déclenche une écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3990">CVE-2016-3990</a>

<p>Un dépassement de tampon basé sur le tas dans la fonction
horizontalDifference8 dans tif_pixarlog.c dans LibTIFF 4.0.6 et antérieur
permet à des attaquants distants de provoquer un déni de service (plantage)
ou d'exécuter du code arbitraire à l'aide d'une image TIFF contrefaite, ce
qui conduit à une écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9533">CVE-2016-9533</a>

<p>tif_pixarlog.c dans libtiff 4.0.6 a des vulnérabilités d'écriture
hors limites dans des tampons alloués sur le tas. Signalé en tant que
MSVR 35094, aussi connu sous l'appellation « dépassement de tampon de tas pour
horizontalDifference dans PixarLog ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9534">CVE-2016-9534</a>

<p>tif_write.c dans libtiff 4.0.6 à un problème dans le chemin du code
d'erreur de TIFFFlushData1() qui ne réinitialisait pas les membres tif_rawcc
et tif_rawcp. Signalé en tant que MSVR 35095, aussi connu sous l'appellation
« dépassement de tampon basé sur le tas dans TIFFFlushData1 ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9535">CVE-2016-9535</a>

<p>tif_predict.h et tif_predict.c dans libtiff 4.0.6 ont des assertions
qui peuvent conduire à des échecs d’assertions en mode débogage, ou à des dépassements
de tampons en mode <q>release</q>, lors de la gestion de tailles de tuile non usuelles
comme pour le sous-échantillonnage YCbCr. Signalé en tant que MSVR 35105, aussi
connu sous l'appellation « dépassement de tampon basé sur le tas dans Predictor ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9536">CVE-2016-9536</a>

<p>tools/tiff2pdf.c dans libtiff 4.0.6 a des vulnérabilités d'écriture
hors limites dans des tampons alloués dans le tas dans t2p_process_jpeg_strip().
Signalé en tant que MSVR 35098, aussi connu sous l'appellation
« dépassement de tampon basé sur le tas dans t2p_process_jpeg_strip ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9537">CVE-2016-9537</a>

<p>tools/tiffcrop.c dans libtiff 4.0.6 a des vulnérabilités d'écriture
hors limites dans des tampons. Signalé en tant que MSVR 35093, MSVR 35096
et MSVR 35097.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9538">CVE-2016-9538</a>

<p>tools/tiffcrop.c dans libtiff 4.0.6 accède à un tampon non défini dans
readContigStripsIntoBuffer() à cause d'un dépassement d'entier uint16.
Signalé en tant que MSVR 35100.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9540">CVE-2016-9540</a>

<p>tools/tiffcp.c dans libtiff 4.0.6 a une écriture hors limites sur des
images tuilées avec une largeur de tuile ne correspondant pas à la largeur de
l'image. Signalé en tant que MSVR 35103, aussi connu sous l'appellation
un dépassement de tampon basé sur le tas dans cpStripToTile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10092">CVE-2016-10092</a>

<p>dépassement de tampon basé sur le tas dans tiffcrop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10093">CVE-2016-10093</a>

<p>dépassement de uint32 par le bas ou par le haut pouvant provoquer un dépassement de
tampon basé sur le tas dans tiffcp</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5225">CVE-2017-5225</a>

<p>LibTIFF version 4.0.7 est vulnérable à un dépassement de tampon basé sur
le tas dans tools/tiffcp aboutissant à un déni de service ou à l'exécution
de code à l'aide d'une valeur de BitsPerSample contrefaite.</p></li>

<li>Bug #846837

<p>dépassement de tampon basé sur le tas dans TIFFFillStrip (tif_read.c)</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.0.2-6+deb7u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-795.data"
# $Id: $
