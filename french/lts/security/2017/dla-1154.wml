#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans graphicsmagick.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14103">CVE-2017-14103</a>

<p>Les fonctions ReadJNGImage et ReadOneJNGImage dans coders/png.c dans
GraphicsMagick 1.3.26 ne gèrent pas correctement les pointeurs d’image sous
certaines conditions d’erreur. Cela permet à des attaquants distants de mener
des attaques d’utilisation de mémoire après libération à l'aide d'un fichier
contrefait, relatif à un appel CloseBlob hors d’usage de ReadMNGImage.
REMARQUE : cette vulnérabilité existe à cause d’un correctif incomplet pour
<a href="https://security-tracker.debian.org/tracker/CVE-2017-11403">CVE-2017-11403</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14314">CVE-2017-14314</a>

<p>Une erreur due à un décalage d'entier dans la fonction DrawImage dans
magick/render.c dans GraphicsMagick 1.3.26 permet à des attaquants distants de
provoquer un déni de service (lecture hors limites de tampon basé sur le tas
pour DrawDashPolygon et plantage d'application) à l'aide d'un fichier
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14504">CVE-2017-14504</a>

<p>ReadPNMImage dans coders/pnm.c dans GraphicsMagick 1.3.26 ne garantit pas le
nombre correct de couleurs pour le format XV 332, conduisant à un déréférencement
de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14733">CVE-2017-14733</a>

<p>ReadRLEImage dans coders/rle.c dans GraphicsMagick 1.3.26 gère incorrectement
les en-têtes RLE qui précisent trop peu de couleurs. Cela permet à des
attaquants distants de provoquer un déni de service (lecture hors limites de
tampon basé sur le tas et plantage d'application) à l'aide d'un fichier
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14994">CVE-2017-14994</a>

<p>ReadDCMImage dans coders/dcm.c dans GraphicsMagick 1.3.26 permet à des
attaquants distants de provoquer un déni de service (déréférencement de pointeur
NULL) à l'aide d'une image DICOM contrefaite, relative à la possibilité de
DCM_ReadNonNativeImages de produire une liste d’images sans image fixe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14997">CVE-2017-14997</a>

<p>GraphicsMagick 1.3.26 permet à des attaquants distants de provoquer un déni
de service (allocation excessive de mémoire) à cause d’un dépassement d'entier
par le bas dans ReadPICTImage dans coders/pict.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15930">CVE-2017-15930</a>

<p>Dans ReadOneJNGImage dans coders/png.c dans GraphicsMagick 1.3.26, un
déréférencement de pointeur NULL survient lors du transfert de rendus scanline
JPEG, en relation avec un pointeur PixelPacket.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>,
<a href="https://security-tracker.debian.org/tracker/CVE-2017-15930">CVE-2017-15930</a>
a été corrigé dans la version 1.3.16-1.1+deb7u12. Les autres problèmes de
sécurité ont été corrigés dans la version 1.3.16-1.1+deb7u10 du 10 Oct 2017 dans
la DLA-1130-1 mais cette annonce n’a jamais été publiée, aussi cette annonce
fournit aussi l’avertissement à propos de ces vulnérabilités.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p> Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été résolus dans la
version 1.3.16-1.1+deb7u12</p> de graphicsmagick
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1154.data"
# $Id: $
