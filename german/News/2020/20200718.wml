#use wml::debian::translation-check translation="b51002b9cd5b7dbca0e39ddb2d17bcc495ead21a" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 9 aktualisiert: 9.13 veröffentlicht</define-tag>
<define-tag release_date>2020-07-18</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die dreizehnte (und letzte) Aktualisierung 
seiner Oldstable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Oldstable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Nach dieser Zwischenversion werden das Sicherheits- und Veröffentlichungsteam 
keine weiteren Aktualisierungen für Debian 9 zur Verfügung stellen. Wer 
weiterhin mit Sicherheitsaktualisierungen versorgt werden möchte, muss auf 
Debian 10 umsteigen oder auf <url "https://wiki.debian.org/LTS"> nachsehen, ob 
die genutzten Pakete und Architekturen vom LTS-Projekt für 
Langzeitunterstützung abgedeckt werden.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können.
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Oldstable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction acmetool "Neukompilierung gegen aktuelles golang, um Sicherheitskorrekturen zu übernehmen">
<correction atril "dvi: Gegenmaßnahme für Befehlsinjektion via Dateiname ergriffen [CVE-2017-1000159]; Überlaufprüfungen im TIFF-Backend überarbeitet [CVE-2019-1010006]; tiff: Fehler bei TIFFReadRGBAImageOriented abfangen [CVE-2019-11459]">
<correction bacula "Übergangspaket bacula-director-common eingeführt, damit /etc/bacula/bacula-dir.conf bei der Komplettdeinstallation (apt purge) nicht verloren geht; root zum Eigentümer der PID-Dateien gemacht">
<correction base-files "/etc/debian_version auf die Zwischenveröffentlichung aktualisiert">
<correction batik "Serverseitige Abfragefälschung via xlink:href-Attribute behoben [CVE-2019-17566]">
<correction c-icap-modules "ClamAV 0.102 unterstützen"> 
<correction ca-certificates "Mozilla-CA-Paket auf 2.40 aktualisiert, vertrauensunwürdige Symantec-Wurzelzertifikate auf Blockierliste gesetzt und <q>AddTrust External Root</q> auslaufen lassen; Nur-E-Mail-Zertifikate entfernt">
<correction chasquid "Neukompilierung gegen aktuelles golang, um Sicherheitskorrekturen zu übernehmen">
<correction checkstyle "Problem mit XML External Entity-Injektion behoben [CVE-2019-9658 CVE-2019-10782]">
<correction clamav "Neue Version der Originalautoren [CVE-2020-3123]; Sicherheitskorrekturen [CVE-2020-3327 CVE-2020-3341]">
<correction compactheader "Neue Version der Originalautoren, die kompatibel mit neueren Thunderbird-Veröffentlichungen ist">
<correction cram "Testfehlschläge ignorieren, damit die Kompilierung wieder funktioniert">
<correction csync2 "HELLO-Befehl scheitern lassen, wenn SSL vorausgesetzt wird">
<correction cups "Heap-Puffer-Überlauf behoben [CVE-2020-3898] und <q>die `ippReadIO`-Funktion liest ggf. zu wenig aus einem Erweiterungsfeld</q> [CVE-2019-8842] behoben">
<correction dbus "Neue Veröffentlichung der Originalautoren; Dienstblockade verhindern [CVE-2020-12049]; Use-after-free bei zwei Benutzernamen mit gleicher UID verhindern">
<correction debian-installer "Aktualisierung für das 4.9.0-13er Linux-Kernel-ABI">
<correction debian-installer-netboot-images "Neukompilierung gegen stretch-proposed-updates">
<correction debian-security-support "Unterstützungsstatus mehrerer Pakete aktualisiert">
<correction erlang "Verwendung schwacher TLS-Verschlüsselungen abgestellt [CVE-2020-12872]">
<correction exiv2 "Problem mit Dienstblockade behoben [CVE-2018-16336]; überstrenge Korrektur für CVE-2018-10958 und CVE-2018-10999 nachgearbeitet">
<correction fex "Sicherheitsaktualisierung">
<correction file-roller "Sicherheitskorrektur [CVE-2020-11736]">
<correction fwupd "Neue Veröffentlichung der Originalautoren; CNAME verwenden, um zum richtigen CDN für Metadaten weiterzuleiten; Programmstart nicht abbrechen, wenn die XML-Metadaten-Datei ungültig ist; öffentliche GPG-Schlüssel der Linux Foundation für Firmware und Metadaten hinzugefügt; Metadaten-Grenze auf 10MB angehoben">
<correction glib-networking "Fehlerhafte Identität melden, wenn die Identität nicht gesetzt ist [CVE-2020-13645]">
<correction gnutls28 "Speicherkorrumpierung behoben [CVE-2019-3829]; Speicherleck behoben; Unterstützung für Sitzungstickets mit Null-Länge hinzugefügt, Verbindungsfehler bei TLS1.2-Sitzungen mit einigen Hosting-Anbietern behoben">
<correction gosa "LDAP-Erfolg/-Fehlschlag genauer abprüfen [CVE-2019-11187]; Kompatibilität mit neueren PHP-Versionen überarbeitet; diverse andere Korrekturen zurückportiert; (De-)Serialisierung mit json_encode/json_decode ersetzt, um PHP-Objekt-Injektion zu vermeiden [CVE-2019-14466]">
<correction heartbleeder "Neukompilierung gegen aktuelles golang, um Sicherheitskorrekturen zu übernehmen">
<correction intel-microcode "Einige Microcodes durch ihre Vorversionen ersetzt, um so Hänger beim Bootvorgang auf Skylake-U/Y und Skylake Xeon E3 loszuwerden">
<correction iptables-persistent "Keinen Fehler werfen, wenn modprobe es tut">
<correction jackson-databind "Mehrere Sicherheitsprobleme betreffend BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 and CVE-2019-17267] behoben">
<correction libbusiness-hours-perl "Explizit vierstellige Jahreszahlen benutzen, um Kompilierungs- und Verwendungsprobleme zu lösen">
<correction libclamunrar "Neue stabile Version der Originalautoren; unversioniertes Metapaket hinzugefügt">
<correction libdbi "Aufruf von _error_handler() wieder auskommentiert, um Consumer-Probleme zu beheben">
<correction libembperl-perl "Mit Fehlerseiten von Apache &gt;= 2.4.40 umgehen">
<correction libexif "Sicherheitskorrekturen [CVE-2016-6328 CVE-2017-7544 CVE-2018-20030 CVE-2020-12767 CVE-2020-0093]; Sicherheitskorrekturen [CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; Puffer-Lese-Überlauf [CVE-2020-0182] und Überlauf vorzeichenloser Ganzzahlen behoben [CVE-2020-0198]">
<correction libvncserver "Heap-Überlauf behoben [CVE-2019-15690]">
<correction linux "Neue stabile Veröffentlichung der Originalautoren; ABI auf 4.9.0-13 aktualisiert">
<correction linux-latest "Aktualisierung für 4.9.0-13 Kernel-ABI">
<correction mariadb-10.1 "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2020-2752 CVE-2020-2812 CVE-2020-2814]">
<correction megatools "Unterstützung für das neue Format der mega.nz-Links eingebaut">
<correction mod-gnutls "Veraltete Verschlüsselungsmethoden in der Testsuite vermeiden; Testfehlschläge in Zusammenhang mit Apaches Maßnahme gegen CVE-2019-10092 behoben">
<correction mongo-tools "Neukompilierung gegen aktuelles golang, um Sicherheitskorrekturen zu übernehmen">
<correction neon27 "OpenSSL-bezogene Testfehlschläge nicht als Abbruchfehler bewerten">
<correction nfs-utils "Potenzielle Anfälligkeit für Dateiüberschreibungen behoben [CVE-2019-3689]; nicht dem ganzen Inhalt von /var/lib/nfs den statd-Benutzer als Eigentümer zuweisen">
<correction nginx "Anfälligkeit für Anfrageschmuggel bei gewissen Fehlerseiten behoben [CVE-2019-20372]">
<correction node-url-parse "Pfade und Hosts vor dem Auswerten überprüfen [CVE-2018-3774]">
<correction nvidia-graphics-drivers "Neue stabile Veröffentlichung der Originalautoren; neue stabile Version der Originalautoren; Sicherheitskorrekturen [CVE-2020-5963 CVE-2020-5967]">
<correction pcl "Fehlende Abhängigkeit von libvtk6-qt-dev nachgereicht">
<correction perl "Mehrere Sicherheitsprobleme mit Bezug auf reguläre Ausdrücke behoben [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Anfälligkeit für seitenübergreifendes Skripting beseitigt [CVE-2020-8035]">
<correction php-horde-data "Anfälligkeit für authentifizierte Fernausführung von Code entfernt [CVE-2020-8518]">
<correction php-horde-form "Anfälligkeit für authentifizierte Fernausführung von Code entfernt [CVE-2020-8866]">
<correction php-horde-gollem "Anfälligkeit für seitenübergeifendes Skripting in der Brotkrumen-Ausgabe behoben [CVE-2020-8034]">
<correction php-horde-trean "Anfälligkeit für authentifizierte Fernausführung von Code entfernt [CVE-2020-8865]">
<correction phpmyadmin "Mehrere Sicherheitskorrekturen [CVE-2018-19968 CVE-2018-19970 CVE-2018-7260 CVE-2019-11768 CVE-2019-12616 CVE-2019-6798 CVE-2019-6799 CVE-2020-10802 CVE-2020-10803 CVE-2020-10804 CVE-2020-5504]">
<correction postfix "Neue stabile Veröffentlichung der Originalautoren">
<correction proftpd-dfsg "Umgang mit SSH_MSG_IGNORE-Paketen überarbeitet">
<correction python-icalendar "Python3-Abhängigkeiten überarbeitet">
<correction rails "Potenzielles seitenübergreifendes Skripting durch Javascript-Escape-Helfer abgestellt [CVE-2020-5267]">
<correction rake "Anfälligkeit für Befehlsinjektion beseitigt [CVE-2020-8130]">
<correction roundcube "Problem mit seitenübergreifendem Skripting durch HTML-E-Mails mit einem schädlichen SVG-Element darin beseitigt [CVE-2020-15562]">
<correction ruby-json "Anfälligkeit für unsichere Objekterstellung beseitigt [CVE-2020-10663]">
<correction ruby2.3 "Anfälligkeit für unsichere Objekterstellung beseitigt [CVE-2020-10663]">
<correction sendmail "Suche des Queue-Runner-Kontrollprozesses im <q>split daemon</q>-Modus überarbeitet, <q>NOQUEUE: Verbinden von (null)</q>, Entfernung schlägt fehl, wenn BTRFS genutzt wird">
<correction sogo-connector "Neue version der Originalautoren, kompatibel mit neueren Thunderbird-Versionen">
<correction ssvnc "Schreibvorgang außerhalb der Grenzen [CVE-2018-20020], Endlosschleife [CVE-2018-20021], unsaubere Initialisierung [CVE-2018-20022], potenzielle Dienstblockade [CVE-2018-20024] behoben">
<correction storebackup "Mögliche Anfälligkeit für Privilegieneskalation behoben [CVE-2020-7040]">
<correction swt-gtk "Fehlende Abhängigkeit von libwebkitgtk-1.0-0 nachgereicht">
<correction tinyproxy "PID-Datei anlegen, bevor die Berechtigungen an ein Nicht-root-Benutzerkonto weitergegeben werden [CVE-2017-11747]">
<correction tzdata "Neue stabile Veröffentlichung der Originalautoren">
<correction websockify "Fehlende Abhängigkeit von python{3,}-pkg-resources nachgereicht">
<correction wpa "Möglichkeit für Umgehung des PMF-Verbindungstrennungsschutzes im AP-Modus entfernt [CVE-2019-16275]; Probleme bei MAC-Randomisierung bei manchen Karten behoben">
<correction xdg-utils "Fensternamen überprüfen, bevor er über D-Bus versendet wird; Verzeichnisse mit Leerzeichen im Dateinamen richtig handhaben; bei Bedarf das <q>applications</q>-Verzeichnis erzeugen">
<correction xml-security-c "Längenberechnung in der concat-Methode überarbeitet">
<correction xtrlock "Blockierung (einiger) Multitouch-Geräte bei Sperre überarbeitet [CVE-2016-10894]">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Oldstable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheits-Team hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2017 4005 openjfx>
<dsa 2018 4255 ant>
<dsa 2018 4352 chromium-browser>
<dsa 2019 4379 golang-1.7>
<dsa 2019 4380 golang-1.8>
<dsa 2019 4395 chromium>
<dsa 2019 4421 chromium>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4621 openjdk-8>
<dsa 2020 4622 postgresql-9.6>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4628 php7.0>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4642 thunderbird>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4666 openldap>
<dsa 2020 4668 openjdk-8>
<dsa 2020 4670 tiff>
<dsa 2020 4671 vlc>
<dsa 2020 4673 tomcat8>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4683 thunderbird>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4692 netqmail>
<dsa 2020 4693 drupal7>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4698 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4703 mysql-connector-java>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4706 drupal7>
<dsa 2020 4707 mutt>
<dsa 2020 4711 coturn>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4715 imagemagick>
<dsa 2020 4717 php7.0>
<dsa 2020 4718 thunderbird>
</table>


<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer Kontrolle liegen:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction certificatepatrol "Nicht kompatibel mit neueren Firefox-ESR-Versionen">
<correction colorediffs-extension "Nicht kompatibel mit neueren Thunderbird-Versionen">
<correction dynalogin "Hängt von simpleid ab, das entfernt werden soll">
<correction enigmail "Nicht kompatibel mit neueren Thunderbird-Versionen">
<correction firefox-esr "[armel] Nicht länger unterstützt (benötigt nodejs)">
<correction firefox-esr "[mips mipsel mips64el] Nicht länger unterstützt (benötigt neuere rustc)">
<correction getlive "Defekt wegen Änderungen bei Hotmail">
<correction gplaycli "Defekt wegen Änderungen der Google-API">
<correction kerneloops "Dienst der Originalautoren nicht mehr verfügbar">
<correction libmicrodns "Sicherheitsprobleme">
<correction libperlspeak-perl "Sicherheitsprobleme; unbetreut">
<correction mathematica-fonts "Benötigt nicht verfügbaren Download-Ort">
<correction pdns-recursor "Sicherheitsprobleme; nicht unterstützt">
<correction predictprotein "Hängt von profphd ab, das entfernt werden soll">
<correction profphd "Unbrauchbar">
<correction quotecolors "Nicht kompatibel mit neueren Thunderbird-Versionen">
<correction selenium-firefoxdriver "Nicht kompatibel mit neueren Firefox-ESR-Versionen">
<correction simpleid "Funktioniert nicht mit PHP7">
<correction simpleid-ldap "Hängt von simpleid ab, das entfernt werden soll">
<correction torbirdy "Nicht kompatibel mit neueren Thunderbird-Versionen">
<correction weboob "Unbetreut; in späteren Veröffentlichungen bereits nicht mehr enthalten">
<correction yahoo2mbox "Seit mehreren Jahren defekt">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Oldstable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständigen Listen von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Oldstable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Oldstable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informationen zur Oldstable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail an 
&lt;press@debian.org&gt; oder kontaktieren das Oldstable-Release-Team 
auf Englisch über &lt;debian-release@lists.debian.org&gt;.</p>
