msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:28+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Նիշք"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Փաթեթ"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Միավոր"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Թարգմանիչ"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Թիմ"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Ամսաթիվ"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "Կարգավիճակ"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Տող"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Վրիպակ"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "get-var lang />, as spoken in <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "Անհայտ լեզու"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "Այս էջը գեներացվել է հետևյալ ամսաթվով՝ <get-var date />."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr "Մինչ նշված նիշքերի հետ աշխատելը համոզվեք որ դրանք թարմացված են:"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Section: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "L10n"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Լեզուների ցանկ"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Աստիճան"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Ակնարկներ"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Սխալներ"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "POT նիշքեր"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Ակնկարկ թարգմանիչների մասին"
