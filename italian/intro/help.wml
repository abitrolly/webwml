#use wml::debian::template title="Come posso aiutare Debian?"
#use wml::debian::translation-check translation="ec0e5f6417c9c57e993abde3ee192fd74dfd76b7" maintainer="Giovanni Mascellani"


<p>Se vuoi partecipare allo sviluppo di Debian ci sono tanti
campi nei quali possono aiutare sia utenti esperti che inesperti.</p>

# TBD - Describe requirements per task?
# such as: knowledge of english, experience in area X, etc..


<h3>Scrivendo codice</h3>
<ul>

<li>Puoi pacchettizzare applicazioni con le quali hai esperienza o che consideri
importanti per Debian e diventare il mantenitore di questi pacchetti. Per
maggiori informazioni consulta il <a href="$(HOME)/devel/">Debian Developer's
Corner</a>.</li>

<li>Puoi aiutare a <a href="https://security-tracker.debian.org/tracker/data/report">segnalare</a>,
<a href="$(HOME)/security/audit/">trovare</a> e 
<a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">risolvere</a>
<a href="$(HOME)/security/">problemi di sicurezza</a> nei pacchetti Debian.
Puoi anche contribuire irrobustendo i <a
href="https://wiki.debian.org/Hardening">pacchetti</a>, i
<a href="https://wiki.debian.org/Hardening/RepoAndImages">repository e le
immagini</a> e <a href="https://wiki.debian.org/Hardening/Goals">altre
cose</a>.</li>

<li>Puoi aiutare a mantenere applicazioni che sono già disponibili in Debian,
soprattutto quelle che usi molto e conosci bene: puoi contribuire in questi
pacchetti con patch o informazioni aggiuntive nel
<a href="https://bugs.debian.org/">Bug Tracking System</a>. Puoi anche entrare
direttamente nel mantenimento di un pacchetto diventando un membro di un team
di mantenimento, o lavorare a software che viene sviluppato per Debian
aggregandoti ad un progetto su <a href="https://salsa.debian.org/">Salsa</a>.
</li>

<li>Puoi aiutare a portare Debian su una nuova architettura con la quale hai
esperienza o iniziando un nuovo port o contribuendo ad uno già esistente. Per
maggiori informazioni guarda la <a href="$(HOME)/ports/">lista dei port
disponibili</a>.</li>

<li>Puoi migliorare i servizi legati a Debian <a
href="https://wiki.debian.org/Services">esistenti</a> oppure creare nuovi
servizi <a href="https://wiki.debian.org/Services#wishlist">richiesti</a>
dalla comunità.</li>

</ul>


<h3>Facendo dei test</h3>
<ul>

<li>Puoi semplicemente testare il sistema operativo ed i programmi contenuti
e comunicare ogni bug o problema non ancora noto che trovi, usando il
<a href="https://bugs.debian.org/">Bug Tracking System</a>. Prova anche a leggere
le segnalazioni associate ai pacchetti che usi e, se riesci a riprodurre i
problemi, fornisci maggiori informazioni.</li>

<li>Puoi aiutare <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">facendo
test dell'installatore e delle immagini ISO live</a>.</li>

</ul>


<h3>Supporto agli utenti</h3>
<ul>

# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language

<li>Se sei un utente esperto puoi aiutare altri utenti sulle
<a href="$(HOME)/support#mail_lists">mailing list di utenti</a> (in particolare la
<a href="https://lists.debian.org/debian-italian/">mailing list degli utenti
italiani</a>) o sul canale IRC <tt>#debian</tt> (o, per gli utenti italiani,
<tt>#debian-it</tt>). Per maggiori informazioni sulle possibilità di supporto e
sulle fonti disponibili leggi le <a href="$(HOME)/support">pagine di supporto
</a>.</li>

</ul>


<h3>Traduzione</h3>
<ul>

# TBD - link to translators mailing lists
# Translators, link directly to your group's pages

<li>Puoi aiutare traducendo applicazioni e informazioni collegate a Debian
(pagine web, documentazione) nella tua lingua, coinvolgendoti in un progetto di
traduzione (la discussione in generale si tiene nelle
<a href="https://lists.debian.org/debian-i18n/">mailing list di
internazionalizzazione</a>, vedi in particolare la <a
href="https://lists.debian.org/debian-l10n-italian/">mailing list di
localizzazione italiana</a>). Puoi anche avviare un nuovo gruppo di
internazionalizzazione, se non ce n'è uno per la tua lingua. Per maggiori
informazioni leggi le <a href="$(HOME)/international/">pagine di
internazionalizzazione</a>.</li>

</ul>


<h3>Documentazione</h3>
<ul>

<li>Puoi aiutare scrivendo documentazione lavorando al
<a href="$(HOME)/doc/ddp">Debian Documentation Project</a> o contribuendo al
<a href="https://wiki.debian.org/">Wiki di Debian</a>.</li>

<li>Puoi mettere un tag e categorizzare i pacchetti sul sito <a
href="https://debtags.debian.net/">debtags</a> per semplificare agli
utenti la ricerca dei software.</li>

</ul>


<h3>Eventi</h3>
<ul>

<li>Puoi aiutare anche nello sviluppo della facciata <em>pubblica</em> di Debian
e contribuire al <a href="$(HOME)/devel/website/">sito Web</a> o aiutando
nell'organizzazione di <a href="$(HOME)/events/">eventi</a> in tutto il mondo.
</li>

<li>Puoi aiutare Debian a pubblicizzarsi, parlandone o facendola vedere ad altri
utenti.</li>

<li>Puoi creare od organizzare un <a href="https://wiki.debian.org/LocalGroups">gruppo
Debian locale</a> con incontri regolari e/o altre attività.</li>

<li>Puoi aiutare con la <a href="https://debconf.org/">conferenze Debian</a>,
facendo la <a href="https://wiki.debconf.org/wiki/Videoteam">registrazione video
dei seminari</a>, <a href="https://wiki.debconf.org/wiki/FrontDesk">accogliendo
i partecipanti al loro arrivo</a>, <a
href="https://wiki.debconf.org/wiki/Talkmeister">supportando i relatori prima
dei loro interventi</a>, con gli eventi speciali (come i  cheese party e wine
party), l'allestimento, la smobilitazione e altre attività.</li>

<li>Puoi aiutare a organizzare la <a href="https://debconf.org/">conferenze Debian</a>,
le mini-DebConf regionali, le <a
href="https://wiki.debian.org/DebianDay">feste per il Debian Day</a>, le 
<a href="https://wiki.debian.org/ReleaseParty">feste per il rilascio</a>, le
<a href="https://wiki.debian.org/BSP">feste per di bug squashing</a>, gli
<a href="https://wiki.debian.org/Sprints">sprint di sviluppo</a> e
<a href="https://wiki.debian.org/DebianEvents">altri eventi</a> in tutto
il mondo.</li>

</ul>


<h3>Donazioni</h3>
<ul>

<li>Puoi <a href="$(HOME)/donations">donare denaro, hardware o servizi</a> al progetto 
Debian in modo che ne possano beneficiare gli utenti e gli sviluppatori. 
Noi cerchiamo costantemente <a href="$(HOME)/mirror/">mirror in tutto il mondo</a>, 
che i nostri utenti possano sfruttare, e <a href="$(HOME)/devel/buildd/">sistemi automatici
di compilazione</a>, per i nostri porter.</li>

</ul>


<h3>Usando Debian!</h3>
<ul>

<li>Puoi <a href="https://wiki.debian.org/ScreenShots">fare
delle istantanee</a> dei pacchetti e <a
href="https://screenshots.debian.net/upload">caricarle</a> su
<a href="https://screenshots.debian.net/">screenshots.debian.net</a>
in modo che agli altri utenti possano vedere com'è il software in
Debian prima di usarlo.</li>

<li>Puoi attivare l'<a href="https://packages.debian.org/popularity-contest">invio
dei dati del popularity-contest</a> per farci sapere quali sono i pacchetti più
popolari e più utili.</li>

</ul>


<p>Come vedi, ci sono molti modi in cui puoi coinvolgerti nel progetto
Debian e solo pochi di essi richiedono che tu sia uno sviluppatore Debian.
Molti progetti possiedono meccanismi che permettono l'accesso diretto ai codici
sorgente a coloro che hanno dimostrato di essere affidabili e competenti.
Tipicamente coloro che possono buttarsi completamente in Debian <a
href="$(HOME)/devel/join">si aggregano al progetto</a>, ma questo non è sempre
necessario.</p>


<h3>Organizzazioni</h3>

<p>
Le organizzazioni educative, commerciali, no-profit o governative possono
essere interessate ad auitare Debian utilizzando le proprie risorse.
Le organizzazioni possono <a href="https://www.debian.org/donations">fare
donazioni</a>, <a href="https://www.debian.org/partners/">formalizzare
delle collaborazioni continuative con noi</a>, <a
href="https://www.debconf.org/sponsors/">sponsorizzare
le nostre conferenze</a>, <a href="https://wiki.debian.org/MemberBenefits">fornire
gratuitamente prodotto o servizi a chi contribuisce a Debian</a>, <a
href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">fornire
gratuitamente hosting per i servizi Debian sperimentali</a>, attivare dei
mirror per i nostri <a href="https://www.debian.org/mirror/ftpmirror">software</a>,
<a href="https://www.debian.org/CD/mirroring/">supporti per l'installazione</a>
o <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">video delle conferenze</a>
o promuovere il nostro software e la comunità con 
<a href="https://www.debian.org/users/">un testimonial</a>
o vendendo <a href="https://www.debian.org/events/merchandise">merchandise</a> Debian,
<a href="https://www.debian.org/CD/vendors/">supporti per l'installazione</a>,
<a href="https://www.debian.org/distrib/pre-installed">sistemi pre-installati</a>,
<a href="https://www.debian.org/consultants/">consulenze</a> o
<a href="https://wiki.debian.org/DebianHosting">hosting</a>.
</p>

<p>
È anche possibile aiutare incoraggiando il proprio personale a partecipare
alla nostra comunità mostrando loro Debian per esempio utilizzando il nostro
sistema operativo nella organizzazione, facendo istruzione sul sistema
operativo e la comunità Debian, dando direttive di contribuire durante il
loro lavoro o inviandoli a uno dei nostri <a href="$(HOME)/events/">eventi</a>.
</p>

# <p>Related links:
