#use wml::debian::template title="데비안 파생"
#use wml::debian::translation-check translation="1b62dcf348569f7b3d0975c674a776e714dc31fc" maintainer="Sebul"

<p>
데비안 기반  <a href="#list">배포판이 많이</a> 있습니다.
어떤 사람들은 공식 데비안 릴리스 외에 <em>추가적으로</em> 이런 배포판을 보고 싶어할 수 있습니다.
</p>

<p>데비안 파생은 데비안에서 수행한 작업을 기반으로 하는 배포판이지만 고유한 정체성, 목표 및 대상을 가지고 있으며 
데비안과 독립된 요소에 의해 생성됩니다.
파생은 자신이 설정한 목표를 달성하기 위해 데비안을 변경합니다.
</p>

<p>
데비안은 데비안 기반 새 배포판을 개발하려는 조직을 환영하고 장려합니다.
데비안의 <a href="$(HOME)/social_contract">사회 계약</a>에 따라, 
우리는 파생이 데비안 및 업스트림 프로젝트에 그들의 작업에 기여하여 모든 사람이 개선의 잇점을 얻기 바랍니다.
</p>

<h2 id="list">어떤 파생이 가능한가?</h2>

<p>
다음의 데비안 파생을 강조하고 싶습니다:
</p>

## Please keep this list sorted alphabetically
## Please only add derivatives that meet the criteria below
<ul>
    <li>
      <a href="https://grml.org/">Grml</a>:
시스템 관리자를 위한 라이브 시스템.
      <a href="https://wiki.debian.org/Derivatives/Census/Grml">더 많은 정보</a>.
    </li>
    <li>
      <a href="https://www.kali.org/">Kali Linux</a>:
보안 감사 및 침투 테스트.
      <a href="https://wiki.debian.org/Derivatives/Census/Kali">더 많은 정보</a>.
    </li>
    <li>
      <a href="https://pureos.net/">Purism PureOS</a>:
	  <a href="https://www.fsf.org/news/fsf-adds-pureos-to-list-of-endorsed-gnu-linux-distributions-1">FSF-endorsed</a>
개인정보, 보안 및 편의에 중점을 둡니다.
      <a href="https://wiki.debian.org/Derivatives/Census/Purism">더 많은 정보</a>.
    </li>
    <li>
      <a href="https://tails.boum.org/">Tails</a>:
사생활 및 익명성 유지.
      <a href="https://wiki.debian.org/Derivatives/Census/Tails">더 많은 정보</a>.
    </li>
    <li>
      <a href="https://www.ubuntu.com/">Ubuntu</a>:
전세계 리눅스 대중화.
      <a href="https://wiki.debian.org/Derivatives/Census/Ubuntu">더 많은 정보</a>.
    </li>
</ul>

<p>
게다가, 데비안 기반 배포판은 
<a href="https://wiki.debian.org/Derivatives/Census">데비안 파생 센서스</a>
및 <a href="https://wiki.debian.org/Derivatives#Lists">다른 곳</a>에 나열되어 있습니다.
</p>

<h2>데비안 대신 파생을 쓰는 까닭</h2>

<p>
파생이 더 나은 특정 요구를 충족시키는 경우 데비안 대신 이를 쓰는 것이 좋습니다.
</p>

<p>
특정 커뮤니티 또는 사람들 그룹에 속해 있고 해당 그룹 사람들을 위한 파생이 있으면 데비안 대신 이를 사용하는 것이 좋습니다.
</p>

<h2>데비안이 파생에 관심있는 까닭</h2>

<p>
파생은 현재 도달하는 청중보다 더 다양한 경험과 요구 사항을 가진 많은 사람들에게 데비안을 데려다줍니다.
파생과의 관계를 개발함으로써 <a href="https://wiki.debian.org/Derivatives/Integration">통합</a> 데비안 인프라에 그들에 대한 정보를 그들이 데비안에 다시 만든 변경 사항을 병합, 
우리는 데비안을 개선, 잠재적으로 데비안 공동체를 확장 우리의 파생 상품과 관객에 대한 우리의 이해를 확장, 
우리의 유도체 우리의 경험을 공유 기존 관객과 데비안을 보다 다양한 관객에게 적합하게 만듭니다.
</p>

<h2>데비안은 어떤 파생을 강조하는가?</h2>

## Examples of these criteria are in the accompanying README.txt
<p>
위에 강조 표시된 파생은 각각 다음 기준 중 대부분을 충족했습니다:
</p>

<ul>
    <li>데비안과 적극적으로 협력</li>
    <li>적극적으로 유지</li>
    <li>적어도 한 명의 데비안 멤버를 포함하여 관련 팀으로 구성</li>
    <li>데비안 파생 상품 센서스에 가입하고 센서스 페이지에 source.list를 포함</li>
    <li>특징이나 초점이 있다</li>
    <li>주목할 만하고 확립된 배포판</li>
</ul>

<h2>왜 데비안에서 파생하는가?</h2>

<p>
패키징 형식, 저장소, 기본 패키지 및 기타 항목을 지정하고 사용할 수 있기 때문에 
처음부터 시작하는 것보다 데비안과 같은 기존 배포판을 변경하는 것이 더 빠를 수 있습니다.
많은 소프트웨어가 패키지되어 있으므로 대부분을 패키지하는 데 시간을 들일 필요가 없습니다.
이를 통해 파생은 특정 고객의 요구에 집중할 수 있습니다.
</p>

<p>
데비안은 우리가 배포하는 것이 파생이 독자에게 수정하고 재배포 할 수 있는 <a href="$(HOME)/intro/free">자유</a>를 보장합니다.
<a href="$(HOME)/social_contract#guidelines">Debian Free Software Guidelines (DFSG)</a>에 따라 
배포한 소프트웨어 라이선스를 확인하여 이를 수행합니다.
</p>

<p>
데비안은 파생의 배포 기반으로 사용할 수 있는 여러 <a href="$(HOME)/releases/">릴리스</a> 주기를 가지고 있습니다.
이를 통해 파생 제품은 <a href="https://wiki.debian.org/DebianExperimental">실험</a> 소프트웨어를 시험하고, 
매우 <a href="$(HOME)/releases/unstable/">빠르게</a> 움직이며, 
품질 보증으로 <a href="$(HOME)/releases/testing/">자주</a> 업데이트 하고, 
업무를 위한 견고한 기반을 확보하고, <a href="$(HOME)/releases/stable/">견고한 기반</a> 위에 
<a href="https://backports.debian.org/">새로운</a> 소프트웨어를 사용하며, 
<a href="$(HOME)/security/">보안</a> 지원을 즐기고
해당 지원을 <a href="https://wiki.debian.org/LTS">확장</a>할 수 있습니다.
</p>

<p>
데비안은 다양한 <a href="$(HOME)/ports/">아키텍처</a>를 지원하며 
기여자들은 새로운 프로세서 유형을 위한 새로운 아키텍처를 자동으로 생성하는 방법을 <a href="https://wiki.debian.org/DebianBootstrap">연구</a>합니다.
이를 통해 파생은 원하는 하드웨어를 사용하거나 새로운 프로세서 설계를 지원할 수 있습니다.
</p>

<p>
데비안 커뮤니티와 기존 파생으로부터 온 사람들은 자신의 작업에서 새로운 배포판을 안내할 수 있습니다.
</p>

<p>
파생은 여러 까닭
(새로운 언어로 번역, 
특정 하드웨어 지원, 
다른 설치 메커니즘 또는 특정 커뮤니티 또는 
사람들의 그룹 지원 등)으로 생성됩니다.
</p>

<h2>데비안에서 파생하려면?</h2>

<p>
필요한 경우 파생은 저장소 같은 데비안 인프라의 일부를 사용할 수 있습니다.
파생물은 로고, 이름 등의 데비안 및 웹 사이트 및 BTS와 같은 데비안 서비스에 대한 참조를 변경해야 합니다.
</p>

<p>
설치할 패키지 세트를 정의하는 것이 목표라면
 데비안에서 <a href="$(HOME)/blends/">데비안 블렌드</a>를 만드는 것이 흥미로운 방법이 될 겁니다.
</p>

<p>
자세한 개발 정보는 <a href="https://wiki.debian.org/Derivatives/Guidelines">지침</a>에 있으며 
안내는 <a href="https://wiki.debian.org/DerivativesFrontDesk">프런트 데스크</a>에서 제공합니다.
</p>
