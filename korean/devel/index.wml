#use wml::debian::template title="데비안 개발자 코너" BARETITLE="true"
#use wml::debian::translation-check translation="2a91a8cd41f14166dc4ef23023d68bc2825268ff" maintainer="Seunghun Han (kkamagui)"

<p>이 페이지의 정보는 공개되어 있지만, 주로 데비안 개발자가 관심있는 내용입니다.
</p>

<ul class="toc">
<li><a href="#basic">기본 사항</a></li>
<li><a href="#packaging">패키징</a></li>
<li><a href="#workinprogress">작업 진행 과정</a></li>
<li><a href="#projects">프로젝트</a></li>
<li><a href="#miscellaneous">기타</a></li>
</ul>

<div id="main">
  <div class="cardleft" id="basic">
  <h2>기본 사항</h2>
      <div>
      <dl>
        <dt><a href="$(HOME)/intro/organization">데비안 조직</a></dt>

        <dd>데비안에는 많은 접근점이 있고 관련된 사람도 많습니다.
이 페이지는 데비안의 특정 측면에 관해 연락할 수 있는 사람을 설명하고 대응할 수 있는 사람을 알려줍니다.
        </dd>

        <dt>구성원</dt>
        <dd>
        데비안은 전세계에 흩어진 많은 사람들이 협동해서 만듭니다.
        <em>패키징 작업</em>은 일반적으로 <a
        href="https://wiki.debian.org/DebianDeveloper">데비안 디벨로퍼
        (Debian Developer, DD)</a>(데비안 프로젝트 정회원)와 <a
        href="https://wiki.debian.org/DebianMaintainer">데비안 메인테이너
        (Debian Maintainer, DM)</a>가 함께 기여합니다.
        데비안 디벨로퍼와 메인테이너는
        <a href="https://nm.debian.org/public/people/dd_all">데비안
        디벨로퍼 목록</a>과 <a
        href="https://nm.debian.org/public/people/dm_all">데비안 메인테이너
        목록</a>에서 이들이 관리하는 패키지와 함께 찾아 볼 수 있습니다.

        <p>또한
        <a href="developers.loc">데비안 개발자 세계지도</a>도 볼 수 있고,
        다양한 데비안 행사의 <a href="https://gallery.debconf.org/">이미지
        갤러리</a>도 볼 수 있습니다.
        </p>
        </dd>

        <dt><a href="join/">데비안 참여</a></dt>

        <dd>
        데비안 프로젝트는 자원봉사자로 구성되며, 우리는 전반적으로 기술적 지식과
        자유 소프트웨어에 대한 관심, 여유시간이 있는 신규 개발자를 찾고 있습니다.
        여러분도 데비안을 도울 수 있으며, 위의 관련 페이지를 보면 됩니다.
        </dd>

        <dt><a href="https://db.debian.org/">개발자 데이터베이스</a></dt>
        <dd>
        데이터베이스에는 모두가 접근 가능한 기본 데이터가 있으며,
        좀 더 사적인 데이터는 다른 개발자들만 볼 수 있습니다.
        로그인 하려면 <a href="https://db.debian.org/">SSL 버전</a>을 써서
        접근하세요.

        <p>데이터베이스를 이용하여
        <a href="https://db.debian.org/machines.cgi">프로젝트 머신</a>,
        <a href="extract_key">개발자의 GPG 키 얻기</a>,
        <a href="https://db.debian.org/password.html">암호 바꾸기</a>,
        데비안 계정의 <a href="https://db.debian.org/forward.html">메일
        포워딩 어떻게 설정하는지 배우기</a>의 목록을 볼 수 있습니다.
        </p>

        <p>데비안 머신 중 하나를 사용하려면 <a href="dmup">데비안 머신 사용
        정책</a>을 반드시 읽어야 합니다.</p>
        </dd>

        <dt><a href="constitution">데비안 헌법</a></dt>
        <dd>
        조직에 가장 중요한 문서로, 프로젝트에서 공식적 의사 결정을 위한 조직
        구조를 설명합니다.
        </dd>

        <dt><a href="$(HOME)/vote/">투표 정보</a></dt>
        <dd>
        리더를 어떻게 뽑는지, 로고를 어떻게 고르고 보통 우리가 어떻게 선거를
        하는지에 관한 여러분이 알고 싶은 모든 것이 들어 있습니다.
        </dd>
     </dl>

# this stuff is really not devel-only
     <dl>
        <dt><a href="$(HOME)/releases/">릴리스</a></dt>

        <dd>
        이는 과거와 현재 릴리스 목록이며, 그중 일부는 별도 웹 페이지에 자세한
        정보가 있습니다.

        <p>여러분은 직접 <a 
        href="$(HOME)/releases/stable/">안정(stable) 릴리스</a>와
        <a href="$(HOME)/releases/testing/">테스트(testing) 배포판</a>
        웹 페이지로 이동할 수 있습니다.</p>
        </dd>

        <dt><a href="$(HOME)/ports/">다른 아키텍처</a></dt>

        <dd>
        데비안은 많은 종류의 컴퓨터에서 실행되며, Intel과 호환되는 것은
        단지 <em>첫 번째</em> 종류일 뿐입니다.
        우리 &lsquo;포트(Ports)&rsquo;의 관리자들은 몇 가지 유용한 웹 페이지를
        가지고 있습니다.
        살펴 보세요. 별난 이름의 또 다른 금속 하나(다른 아키텍처 머신)가
        갖고 싶을 수도 있습니다.
	</dd>
      </dl>
      </div>

  </div>

  <div class="cardright" id="packaging">
     <h2>패키징</h2>
     <div>

      <dl>
        <dt><a href="$(DOC)/debian-policy/">데비안 정책 매뉴얼</a></dt>
        <dd>
        이 매뉴얼은 데비안 배포판의 정책 요구 사항을 설명합니다.
        여기에는 데비안 아카이브의 구조와 내용, 운영 체제의 몇 가지 설계 문제
        및 각 패키지가 배포판에 포함되기 위해 충족해야 하는 기술적 요구 사항을
        포함합니다.

        <p>간단히 말해서 여러분은 그것을 읽을 <strong>필요</strong>가 있습니다.</p>
        </dd>
      </dl>

      <p>여러분이 관심 있을 정책과 관련된 여러 문서는 다음과 같습니다.</p>
      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">파일 시스템 계층 표준</a>
        (Filesystem Hierarchy Standard, FHS)
        <br />FHS는 어떤 것이 들어가야만 하는 디렉터리 또는 파일의 목록이며,
             이와 호환성은 정책 3.x에 필요합니다.</li>
        <li><a href="$(DOC)/packaging-manuals/build-essential">빌드 필수 패키지</a> 목록
        <br />빌드 필수 패키지는 어떤 패키지를 빌드 하기 전에 설치되어
            있어야할 패키지나, 여러분 패키지의 <code>Build-Depends</code>
            라인에 포함할 필요가 없는 패키지 세트를 말합니다.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">메뉴 시스템</a>
        <br />일반적인 작업을 위해 특별한 명령줄 인수를 전달할 필요가 없는
            인터페이스가 있는 프로그램은 등록된 메뉴 항목이 있어야 합니다.
            <a href="$(DOC)/packaging-manuals/menu.html/">메뉴 시스템 문서</a>도
            확인하세요.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacs 정책</a>
        <br />Emacs와 관련된 패키지는 자체 하위 정책 문서를 준수해야 합니다.</li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Java 정책</a>
        <br />Java 관련 패키지에 대해 위와 동일한 정책이 적용됩니다.</li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perl 정책</a>
        <br />Perl 패키징에 관한 모든 것을 다루는 하위 정책입니다.</li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Python 정책</a>
        <br />Python 패키지에 관한 모든 것을 다루는 하위 정책입니다.</li>
#	<li><a href="https://pkg-mono.alioth.debian.org/cli-policy/">Debian CLI Policy</a>
#	<br />Basic policies regarding packaging Mono, other CLRs and
#        CLI based applications and libraries</li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf
        명세</a>
        <br />"debconf" 구성 관리 서브시스템에 대한 규격입니다.</li>
#        <li><a href="https://dict-common.alioth.debian.org/">Spelling dictionaries and tools policy</a>
#        <br />Sub-policy for <kbd>ispell</kbd> / <kbd>myspell</kbd> dictionaries and word lists.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft/html/">Webapps Policy Manual</a> (draft)
#	<br />Sub-policy for web-based applications.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft-php/html/">PHP Policy</a> (draft)
#	<br />Packaging standards of PHP.</li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">데이터베이스
        응응프로그램 정책</a> (초안)
	<br />데이터베이스 응용 프로그램 패키지에 대한 지침 및 모범 사례
            모음입니다.</li>
	<li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">
        Tcl/Tk 정책</a> (초안)
	<br />Tcl/Tk 패키징 관련 모든 내용을 다루는 하위 정책입니다.</li>
	<li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">
        Ada를 위한 데비안 정책</a>
	<br />Ada 패키징 관련 모든 내용을 다루는 하위 정책입니다.</li>
      </ul>

      <p><a href="https://bugs.debian.org/debian-policy">정책에 제안된 변경
      내용</a>도 보세요.</p>

      <p>이전 패키징 매뉴얼은 대부분 최신 정책 매뉴얼에 통합되어 있다는 점을
      유의하세요.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">
        개발자 레퍼런스</a></dt>

        <dd>
        이 문서의 목적은 데비안 개발자에게 권장 절차와 사용 가능 자원에
        대한 개요를 제공하는 겁니다. 다음은 또 다른 필독서입니다.
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">신임 메인테이너 안내서</a></dt>

        <dd>
        이 문서는 데비안 패키지 빌드를 공용어(영어)로 설명하며, 실습 예제로 잘
        채워져 있습니다. 만약 개발자, 즉 패키징 하는 사람이 되고 싶다면,
        아주 확실하게 이 문서를 보고 싶을 겁니다.
        </dd>
      </dl>
      </div>

  </div>

  <div class="card" id="workinprogress">
      <h2>작업 진행 과정</h2>
      <div>

	<dl>
        <dt><a href="testing">테스트(testing) 배포판</a></dt>
        <dd>
        &lsquo;테스트&rsquo; 배포판은 여러분이 받을 필요가 있는 패키지가 들어
        있으며, 다음 데비안 릴리스를 위해 패키지 릴리스를 고려하기 위해
        존재합니다.
        </dd>

        <dt><a href="https://bugs.debian.org/release-critical/">릴리스의 심각한
        버그</a></dt>

        <dd>
        이는 "테스트(testing)" 배포판에서 패키지가 제거될 지도 모르거나,
        어떤 경우에는 배포판 릴리스가 지연되기도 하는 버그 목록입니다.
        목록에서 &lsquo;심각&rsquo; 이상의 심각도를 가진 버그 리포트는
        가능한 한 빨리 패키지의 버그를 수정하세요.
        </dd>

        <dt><a href="$(HOME)/Bugs/">버그 추적 시스템</a></dt>
        <dd>
        데비안 버그 추적 시스템(Bug Tracking System, BTS)은 버그를 보고하고,
        토론하고, 고치는 곳입니다. 버그 추적 시스템은 데비안과 관련된 대부분의
        문제 보고를 환영합니다.
        BTS는 사용자와 개발자 모두에게 쓸모가 있습니다.
        </dd>

        <dt>개발자 관점에서 패키지 개요</dt>
        <dd>
        <a href="https://qa.debian.org/developer.php">패키지 정보</a>와
        <a href="https://packages.qa.debian.org/">패키지 추적기</a> 웹 페이지는
        메인테이너에게 값진 정보를 모아 줍니다.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources#pkg-tracker">
        패키지 추적기</a></dt>
        <dd>
        다른 패키지의 최신 상태를 유지하고자 하는 개발자를 위해, 패키지
        추적기는 이메일로 BTS 메일과 알림을 보내주는 서비스를 가입할 수 있게
        해줍니다. 서비스는 구독한 패키지와 관련된 업로드 및 설치 관련 정보를
        보내줍니다.
        </dd>

        <dt><a href="wnpp/">도움이 필요한 패키지</a></dt>
        <dd>
        작업이 필요하고 잠재적인 패키지(Work-Needing and Prospective Packages,
        WNPP)는 새로운 메인테이너가 필요한 데비안 패키지의 목록이며, 또한
        데비안에 아직 포함되지 않은 패키지의 목록이기도 합니다. 패키지를
        만들거나 입양하거나 고아로 만들려면 확인하세요.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">\
            유입(Incoming) 시스템</a></dt>
        <dd>
        신규 패키지는 내부 아카이브 서버에 있는 "Incoming" 시스템 안으로
        들어갑니다.
        수락된 패키지는 거의 즉시 <a
        href="https://incoming.debian.org/">HTTP로 접근</a>할 수 있으며,
        하루에 4번 <a href="$(HOME)/mirror/">미러 서버</a>로 전파됩니다.
        <br />
        <strong>주의</strong>: 유입의 특성상, 패키지를 미러링하는 건 권장하지
        않습니다.
        </dd>

        <dt><a href="https://lintian.debian.org/">Lintian 보고서</a></dt>

        <dd>
        <a href="https://packages.debian.org/unstable/devel/lintian">
        Lintian</a>은 패키지가 정책에 부합하는지 확인하는 프로그램입니다.
        매 업로드 전에 Lintian을 사용해야 하며, 앞서 언급한 페이지에 배포판의
        모든 패키지에 대한 보고서가 있습니다.
        </dd>

        <dt><a href="https://wiki.debian.org/HelpDebian">데비안 돕기</a></dt>
        <dd>
        데비안 위키는 개발자와 다른 기여자를 위한 조언을 모으고 있습니다.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">\
            실험(experimental) 배포판</a></dt>
        <dd>
        <em>실험(experimental)</em> 배포판은 고도로 실험적인 소프트웨어의 임시
        대기지역으로 사용됩니다.
        <a href="https://packages.debian.org/experimental/"><em>실험</em>의
        패키지</a>는 <em>불안정(unstable)</em>을 어떻게 사용하는지 알 때만 쓰세요.
        </dd>
      </dl>
      </div>

  </div>
  <div class="card" id="projects">
     <h2>프로젝트</h2>
     <div>

      <p> 데비안은 큰 그룹이며, 여러 내부 그룹과 프로젝트 같은 것으로
      구성됩니다. 아래는 시간순으로 정렬된 웹 페이지입니다.</p>
      <ul>
          <li><a href="website/">데비안 웹 페이지</a></li>
          <li><a href="https://ftp-master.debian.org/">데비안 아카이브</a></li>
          <li><a href="$(DOC)/ddp">데비안 문서 프로젝트 (Debian Documentation
          Project, DDP)</a></li>
          <li><a href="https://qa.debian.org/">품질 보증</a> 그룹</li>
          <li><a href="$(HOME)/CD/">데비안 CD 이미지</a></li>
          <li><a href="https://wiki.debian.org/Keysigning">키 사이닝(Key Signing)
              협력 페이지</a></li>
          <li><a href="https://wiki.debian.org/DebianIPv6">데비안 IPv6
          프로젝트</a></li>
          <li><a href="buildd/">자동화 빌드 네트워크</a> 및 관련
          <a href="https://buildd.debian.org/">빌드 로그</a></li>
          <li><a href="$(HOME)/international/l10n/ddtp">데비안 설명 번역
          프로젝트 (Debian Description Translation Project, DDTP)</a></li>
	  <li><a href="debian-installer/">데비안 설치관리자</a></li>
	  <li><a href="debian-live/">데비안 라이브</a></li>
	  <li><a href="$(HOME)/women/">데비안 여성</a></li>
	  <li><a href="$(HOME)/blends/">데비안 퓨어 블랜드(Pure Blends)</a></li>
	</ul>
	</div>

  </div>

  <div class="card" id="miscellaneous">
      <h2>기타</h2>
      <div>

      <p>관련 링크:</p>
      <ul>
	  <li>컨퍼런스 발표 <a
          href="https://debconf-video-team.pages.debian.net/videoplayer/">비디오</a>.</li>
        <li><a href="passwordlessssh">암호를 물어보지 않도록 ssh 설정하기</a>.</li>
        <li><a href="$(HOME)/MailingLists/HOWTO_start_list">
        신규 메일링 리스트 요청</a>하기.</li>
        <li><a href="$(HOME)/mirror/">데비안 미러링</a>에 대한 정보.</li>
        <li><a href="https://qa.debian.org/data/bts/graphs/all.png">모든 버그의
        그래프</a>.</li>
	<li><a href="https://ftp-master.debian.org/new.html">데비안에 들어가길
        기다리는 신규 패키지</a> (NEW queue).</li>
        <li><a href="https://packages.debian.org/unstable/main/newpkg">최근
        7일 동안 신규 데비안 패키지</a>.</li>
        <li><a href="https://ftp-master.debian.org/removals.txt">데비안에서
        제거된 패키지</a>.</li>
        </ul>

      </div>

  </div>
</div>
