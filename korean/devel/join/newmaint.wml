#use wml::debian::template title="데비안 새 멤버 코너" BARETITLE="true"
#use wml::debian::translation-check translation="e75c4ef4f01457261f11a81e80de9862be35be30" maintainer="Sebul"

<p>데비안 새 멤버 절차는 공식 데비안 개발자(Debian Developer, DD)가 되는 절차입니다.
예비 데비안 개발자에서 DD로 되기 위한 신청에 대한 모든 상세, 절차의 다른 단계, 
진행중인 신청 절차를 어떻게 따라가는지 이 페이지에서 찾을 수 있습니다.</p>

<p>가장 중요한 것은 데비안을 개선하기 위해 공식 데비안 개발자가 <em>아니어도</em> 된다는 겁니다.
사실, 새 멤버 절차를 신청하기 전에 데비안에 기여한 기록이 먼저 있어야 합니다.</p>

<p><a name="non-maintainer-contributions"></a>데비안은 열린 커뮤니티이며 배포판을 사용하거나 개선하려는 모든 사람을 환영합니다. 
개발자 아닌 여러분이 할 수 있는 것:</p>

<ul> 
  <li><a href="#Sponsor">후원자</a>로 패키지 유지관리</li>
  <li>번역하고 검토하기</li>
  <li>문서 만들고 개선하기</li>
  <li><a href="../website">웹사이트 유지관리 돕기</a></li>
  <li>버그 다루기로 돕기 (패치 제공, 버그 제출 잘 하기, 버그 존재 확인, 문제를 재현할 수 있는 방법 찾기 ...)</li>
  <li>패키징 팀(예. debian-qt-kde 또는 debian-gnome) 활동 멤버</li>
  <li>서브프로젝트(예. debian-installer 또는 debian-desktop) 활동 멤버</li>
  <li>등등</li> 
</ul>

<p><a href="$(DOC)/developers-reference/new-maintainer.html">데비안 개발자 참조</a>에는 몇몇 작업(특히, 후원자를 어떻게 찾을지)을 어떻게 할 지 몇몇 구체적 제안이 들어있습니다.</p>

<p>데비안 새 멤버 절차는 공식 DD가 되는 절차입니다.
이것이 데비안의 전통적인 정식 멤버 역할입니다.
DD는 데비안 선거에 참여할 수 있습니다. 업로드 DD는 모든 패키지를 아카이브에 업로드 할 수 있습니다.
업로드 DD로 신청하기 전에 최소 6개월 동안 패키지를 유지관리한 기록이 있어야 합니다.
예를 들어, <a
href="https://wiki.debian.org/DebianMaintainer">데비안 관리자(DM)</a>로 패키지를 업로드하거나, 팀 내부에서 작업하거나 후원자가 업로드한 패키지를 유지관리합니다.
비 업로드 DD는 아카이브에서 데비안 관리자와 동일한 권한을 갖습니다. 비 업로드 DD로 적용하기 전에 프로젝트 내부의 작업에 대한 눈에 띄고 중요한 트랙 레코드가 있어야 합니다.</p>

<p>새 멤버 절차는 데비안의 품질보증 노력의 일부라는 것을 이해하는 것이 중요합니다.
 데비안 작업에 충분한 시간을 할애할 수 있는 개발자를 찾기는 어렵기 때문에 신청자가 작업을 지속하고 잘 수행할 수 있는지 확인하는 것이 중요합니다.
따라서 우리는 예비 멤버들이 데비안에 적극적으로 참여한 적이 있기를 바랍니다.</p>

<p><a name="developer-priveleges"></a>모든 데비안 개발자는:</p>
<ul>
  <li>데비안 프로젝트의 멤버입니다.</li>
  <li>전체 프로젝트 이슈에 관한 투표를 할 수 있습니다.</li>
  <li>데비안이 돌아가는 대부분의 시스템에 로그인할 수 있습니다.</li>
  <li><em>모든</em> 패키지 업로드 권한이 있습니다 
   (비업로드 개발자는 제외, DM의 업로드 권한).</li>
  <li>debian-private 메일링 리스트에 권한이 있습니다.</li>
</ul>

<p>즉, 데비안 개발자가 되는 것은 프로젝트의 인프라와 관련된 몇 가지 중요한 특권을 부여합니다.
분명히 이것은 신청자에 대한 많은 신뢰와 헌신을 요구합니다.</p>

<p>결과적으로 전체 NM 절차는 매우 엄격하고 철저합니다.
이것은 등록 개발자가 되는 것에 관심이 있는 사람들을 단념시키기 위한 것이 아니라, 새 멤버 절차가 왜 그렇게 많은 시간이 걸리는지 설명합니다.</p>

<p><a href="#Glossary">용어 정의</a>를 읽고 이 페이지의 나머지를 읽으세요.</p>

<p>다음 페이지는 신청자에게 관심있을 겁니다:</p>

<ul>
 <li><a href="nm-checklist">체크리스트 - 신청자에게 필요한 단계</a>
  <ul>
   <li><a href="nm-step1">1단계 : 신청</a></li>
   <li><a href="nm-step2">2단계 : 식별</a></li>
   <li><a href="nm-step3">3단계 : 철학 및 절차</a></li>
   <li><a href="nm-step4">4단계 : 과제 및 기술</a></li>
   <li><a href="nm-step5">5단계 : 추천</a></li>
   <li><a href="nm-step6">6단계 : 프런트 데스크 확인</a></li>
   <li><a href="nm-step7">7단계 : 데비안 계정관리자 확인 및 계정 만들기</a></li>
  </ul></li>
 <li><a href="https://nm.debian.org/public/newnm">참가 신청서</a></li>
</ul>

<p>데비안 개발자이고 새 멤버 절차에 참여하고 싶다면 다음 페이지를 방문하세요.</p>
<ul>
  <li><a href="nm-amchecklist">신청 관리자용 체크리스트</a></li>
  <li><a href="nm-advocate">예비 멤버 옹호</a></li>
  <li><a href="nm-amhowto">신청 관리자용 미니 하우투</a></li>
  <li><a href="$(HOME)/events/keysigning">키 서명 미니하우투</a></li>
</ul>

<p>기타:</p>
<ul>
  <li><a href="https://nm.debian.org/">새 멤버 절차 상태 데이터베이스</a></li>
  <li><a href="https://nm.debian.org/process/">현재 신청자 목록</a></li>
  <li><a href="https://nm.debian.org/public/managers">현재 신청 관리자 목록</a></li>
</ul>

<define-tag email>&lt;<a href="mailto:%0">%0</a>&gt;</define-tag>

<h2><a name="Glossary">용어 정의</a></h2>
<dl>
 <dt><a name="Advocate">옹호자</a>:</dt>
  <dd><a href="#Member">데비안 멤버</a>로 신청을 옹호.
<a href="#Applicant">신청자</a>를 잘 알아야 하며 
신청자의 일, 흥미, 계획에 대해 개요를 줄 수 있어야 합니다.
옹호자는 종종 신청자의 <a href="#Sponsor">후원자</a>입니다.
  </dd>

 <dt><a name="Applicant">신청자</a>, 새 멤버, 역사적으로 새 관리자 (NM):</dt>
  <dd>데비안 개발자로 데비안 멤버십을 요청하는 사람.</dd>

 <dt><a name="AppMan">신청 관리자</a> (AM):</dt>
  <dd><a href="#Member">데비안 멤버</a>로<a 
   href="#Applicant">신청자</a>에 할당 <a href="#DAM">데비안 계정 관리자</a>에 필요한 정보를 모아 신청에 대해 결정합니다. 
한 신청 관리자를 둘 이상의 신청자에게 할당할 수 있습니다.</dd>

 <dt><a name="DAM">데비안 계정 관리자</a> (DAM): <email da-manager@debian.org></dt>
  <dd>데비안 프로젝트 리더 (DPL)에 의해 데비안 계정 생성 및 제거를 관리하기 위해 위임된 <a href="#Member">데비안 멤버</a>. DAM은 신청에 대한 최종 결정을 내립니다.</dd>

 <dt><a name="FrontDesk">프런트 데스크</a>: <email nm@debian.org></dt>
  <dd>프런트 데스크 멤버는 초기 신청, 옹호 메시지 및 최종 신청 보고서를 받고 AM을 NM에 할당하는 등 NM 절차에 대한 인프라 작업을 수행합니다.
  신청에 문제가 발생하면 연락하는 지점.</dd>

 <dt><a name="Member">멤버, 개발자</a>:</dt>
  <dd>데비안 멤버로, 새 멤버 절차를 거치고 신청 허락한 사람.</dd>

 <dt><a name="Sponsor">후원자</a>:</dt>
  <dd><a href="#Member">데비안 멤버</a>로 신청자의 멘토 역할 하는 사람:
신청자가 제공한 패키지를 확인하고 문제를 찾고 패키징을 개선하는 데 도움을 줍니다.
후원자가 패키지에 만족하면 신청자를 대신하여 데비안 아카이브에 패키지를 업로드합니다.
신청자는 패키지 자체를 업로드하지 않음에도 신청자는 그러한 패키지의 관리자로 기록됩니다.</dd>
</dl>
