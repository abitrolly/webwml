#use wml::debian::cdimage title="데비안 CD에 대해 자주 묻는 질문" NOHEADER=true BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="802fdbf2f4ac7abd6d9ec5de0f3645fd3e56f644" maintainer="Seunghun Han (kkamagui)"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<p><strong>%body</strong></p>
</define-tag>

<h1><a name="top">자주 묻는 질문</a></h1>

<toc-display/>



<toc-add-entry name="what-is">도대체 <q>CD 이미지가</q> 뭔가요?</toc-add-entry>

<p>CD 이미지는 바로 CD의 데이터가 일반 컴퓨터 파일로 표현된 형태이며, 인터넷을
통해 전송될 수 있습니다. CD를 굽는 프로그램은 이미지 파일을 사용해서 실제 CD를
만들 수 있습니다.
</p>

<p>CD를 제대로 구웠다면 CD를 열었을 때, <tt>.iso</tt> 파일이 보이면 절대
안됩니다. 대신에 수 많은 파일과 디렉터리가 보여야 하죠. 데비안 CD의 경우는
<q>dists</q> 디렉터리와 <q>README.html</q> 파일이 들어 있습니다.</p>

<p><tt>.iso</tt> 형식은 대략적으로 <tt>.zip</tt> 파일과 비교할 수 있습니다.
iso 형식은 다른 파일과 디렉터리를 포함하고 최종 CD에서만 보이거든요.
몇몇 아카이브 프로그램은 <tt>.iso</tt> 파일을 <q>풀 수 있게</q> 해줍니다.
이러한 기능을 풀린 파일로 CD를 굽는데 사용하지 마세요. 결과물로 생성된 CD는
부팅하는데 실패할 겁니다. 왜냐하면 <tt>.iso</tt> 형식은 CD 부팅과 관련된 특수한
정보를 포함하고 있고, 이 정보가 CD를 풀었을 때 사라지기 때문입니다.
<a href="#record-unix">리눅스</a>나 <a href="#record-windows">윈도우</a>,
<a href="#record-mac">맥 OS</a>에서 CD 이미지를 어떻게 정확히 굽는지를
살펴보세요.</p>

# ============================================================

<toc-add-entry name="mailing-list">제 질문의 답이 FAQ에 없습니다.</toc-add-entry>

<p>질문에 대한 답을 FAQ에서 찾을 수 없다면, 데비안 메일링 리스트에 도움을
요청할 수 있습니다. 어떤 경우라도 메일을 보내기 전에 먼저
<a href="https://lists.debian.org/">메일링 리스트 저장소를 검색</a>하기 바랍니다.
여러분은 메일링 리스트에 <a href="$(HOME)/MailingLists/subscribe">가입</a>하고
<a href="$(HOME)/MailingLists/unsubscribe">탈퇴</a>할 수도 있습니다.
하지만, 메일을 리스트로 보내기 위해 가입할 필요는 없습니다. 가입을 하지
않았다면, 답변을 달 때 여러분을 참조에 넣어달라고 부탁하면 되니까요.</p>

<p>CD 설치 문제와 관련된 메일링 리스트:</p>

<ul>

  <li><a href="https://lists.debian.org/debian-cd/">debian-cd</a>:
  CD 이미지 생성 프로세스, CD 미러 서버, CD 부팅, 새로운 공식 이미지 발표와
  관련된 토론을 합니다.</li>

  <li><a href="https://lists.debian.org/debian-boot/">debian-boot</a>:
  조금 부적절한 명칭이지만, 이 리스트는 실제로 설치 프로세스를 담당합니다.
  CD로 성공적으로 부팅한 이후에 발생하는 문제는 어떤 것이든 debian-cd 리스트보다
  여기가 적합합니다.</li>

  <li><a href="https://lists.debian.org/debian-live/">debian-live</a>:
  데비안 라이브 프로젝트를 위한 리스트입니다. 데비안 라이브 이미지 빌드에
  사용되는 소프트웨어 개발에 중점을 두고 있으나 라이브 이미지 사용과 관련된
  토론에도 적합합니다.</li>

  <li><a href="https://lists.debian.org/debian-user/">debian-user</a>:
  데비안 사용자를 위한 일반적인 지원과 관련된 메일링 리스트입니다. 설치 성공 후
  시스템을 사용할 때 발생하는 문제에 중점을 둡니다.
  비 영어권 사용자를 위한
  <a href="https://lists.debian.org/debian-user-catalan/">카탈루냐어</a>,
  <a href="https://lists.debian.org/debian-chinese-big5/">중국어</a>,
  <a href="https://lists.debian.org/debian-user-danish/">덴마크어</a>,
  <a href="https://lists.debian.org/debian-esperanto/">에스페란토어</a>,
  <a href="https://lists.debian.org/debian-user-french/">프랑스어</a>,
  <a href="https://lists.debian.org/debian-user-german/">독일어</a>,
  <a href="https://lists.debian.org/debian-user-hungarian/">마자르어</a>,
  <a href="https://lists.debian.org/debian-user-indonesian/">인도네시아어</a>,
  <a href="https://lists.debian.org/debian-italian/">이탈리아어</a>,
  <a href="https://lists.debian.org/debian-japanese/">일본어</a>,
  <a href="https://lists.debian.org/debian-user-polish/">폴란드어</a>,
  <a href="https://lists.debian.org/debian-user-portuguese/">포르투갈어</a>,
  <a href="https://lists.debian.org/debian-russian/">러시아어</a>,
  <a href="https://lists.debian.org/debian-user-spanish/">스페인어</a>,
  <a href="https://lists.debian.org/debian-user-swedish/">스웨덴어</a>,
  <a href="https://lists.debian.org/debian-user-turkish/">터키어</a>,
  <a href="https://lists.debian.org/debian-user-ukrainian/">우크라이나어</a>
  리스트도 있습니다.
  </li>

</ul>

# ============================================================

<toc-add-entry name="why-jigdo">왜 <q>jigdo</q> 프로그램을 사용해야 하나요?
저는 간단한 HTTP 다운로드가 더 좋다구요!</toc-add-entry>

<p>현재 .deb 파일로 구성된 완전한 데비안 배포판이 들어있는
<a href="$(HOME)/mirror/list">데비안 미러 서버가 거의 300개</a>나 있습니다.
하지만, <a href="../http-ftp/">아주 소수의</a> 머신이 데비안 CD를 제공하고 있죠.
결과적으로, CD 이미지 서버는 항상 과부하 상태입니다.</p>

<p>게다가, 몇몇 사람들이 접속이 끊어 졌을 때 <em>이어받기</em> 대신에
<em>재시작</em>을 계속 눌러서 엄청난 대역폭이 낭비되기 때문에, 누구도 CD 서버를
더 늘리는데 적극적이지 않습니다.
데비안이 지속적으로 업그레이드되기도 하고 <q>안정(stable)</q> 배포판 대신
<q>테스트(testing)</q>/<q>불안정(unstable)</q> 배포판을 사용하기에 정규
미러 서버가 더 매력적이라는 이유도 있습니다.</p>

<p>Jigdo는 이러한 상황을 300개의 미러 서버 중에서 하나를 골라 CD 이미지를
다운로드하는 방식으로 해결하려 합니다. 하지만, 이러한 미러 서버는 CD 이미지가
아니라 개별 .deb 파일을 가지고 있기에, 작은 .deb 파일을 하나의 큰 CD 이미지로
만들려면 추가적인 작업이 필요합니다.</p>

<p><a href="../jigdo-cd/">Jigdo 사용</a>을 두려워하지 마세요!
CD 이미지를 생성하는 복잡한 프로세스는 완전히 숨겨져 있습니다.
그 대신, 가깝고 CD 서버보다 빠른 300개 데비안 미러 서버의 혜택을 얻을 수
있습니다.</p>

# ============================================================

<toc-add-entry name="which-cd">수많은 이미지 중에서 어떤 걸 다운로드 받야야
하나요? 전부 필요한가요?</toc-add-entry>

<p>아닙니다. 우선 CD<em>나</em> DVD<em>, 또는</em> BD 이미지만 다운로드 하세요.
세 종류의 이미지는 모두 같은 패키지가 들어있습니다.</p>

<p>또한, 여러분 컴퓨터 아키텍처용 CD/DVD/BD 이미지만 필요합니다.
아키텍처는 여러분이 쓰는 컴퓨터의 하드웨어 종류입니다. 지금까지 가장 유명한
것은 인텔/AMD 아키텍처이므로, 대부분의 사람들은 <q>i386</q> 이미지만 원할
겁니다.
여러분의 PC에 64비트 AMD나 인텔 프로세서가 있다면 <q>i386</q>도 좋지만
<q><a href="../../ports/amd64/">amd64</a></q> 이미지가 필요할 겁니다. 그리고
<q><a href="../../ports/ia64/">ia64</a></q> 이미지는 동작하지<em>않을</em>
겁니다.</p>

<p>게다가, 대부분 해당 아키텍처의 이미지 전부를 다운로드할 필요가 없습니다.
패키지는 인기에 따라 정렬되어 있고, 첫 번째 CD/DVD/BD에는 설치 시스템과 가장
인기있는 패키지가 들어 있습니다.
두 번째에는 다소 덜 인기 있는 패키지가 있으며, 네 번째는 인기가 별로 없는
패키지 등이 들어 있죠.
아주 특별한 요구사항이 없는 한, 아마 첫 번째 DVD나 CD 몇 장만 필요할 겁니다.
그리고 CD/DVD/BD에 포함되어 있지 않은 패키지가 필요한 상황이 오면, 항상
인터넷을 통해 설치할 수 있습니다.</p>

<p>네트워크 설치 CD, 업데이트 CD, 소스 CD를 다운로드하길 원하는지 혹은
필요한지를 판단하려면 다음 절을 보세요.</p>

# ============================================================

<toc-add-entry name="netinst"><q>netinst</q>나 <q>네트워크 설치</q>
CD가 뭔가요?</toc-add-entry>

<p><a href="../netinst/">네트워크 설치 페이지</a>를 인용하자면,
<q>네트워크 설치</q>나 <q>netinst</q> CD는 운영체제 전체를 설치할 수 있게
해주는 단일 CD입니다. 이 단일 CD에는 설치를 시작하고 인터넷에서 나머지 패키지를
가져올 수 있는 최소한의 소프트웨어가 들어 있습니다.</p>

<p>고속 인터넷에 연결된 단일 머신에서 단순히 데비안을 설치하고자 한다면,
네트워크 설치는 가장 쉽고 빠른 선택지입니다. 설치하려고 선택한 패키지만
다운로드하고 시간과 대역폭도 모두 절약됩니다.</p>

# ============================================================

<toc-add-entry name="update-cd"><q>업데이트</q> CD/DVD는 뭔가요?</toc-add-entry>

<p>업데이트 CD/DVD에는 안정(stable) 배포판의 주요 릴리스 버전,
즉 7.<strong>0</strong>, 8.<strong>0</strong> 등과 같은 버전 및 이후 릴리스의
변경된 패키지 전부가 들어있습니다.
예를 들자면 <q>debian-8.0.0</q> CD/DVD를 설치했다면 <q>debian-8.0.0</q> 세트를
<q>debian-8.2.0</q> 세트로 바꾸기 위해 <q>debian-update-8.2.0</q> 디스크 세트를
추가할 수 있습니다.</p>

<p>이런 종류의 CD/DVD는 대량의 CD/DVD를 찍어내는 제작자를 위한 것이며,
개별 버전 별로 CD/DVD를 굽는 것보다 비용이 적게 듭니다.
이러한 제작자에게 CD/DVD를 주문한다면, 좀 오래된 릴리스의 CD/DVD를 받을 수
있고 추가적으로 최신 개정판(Revision)을 위한 업데이트 CD/DVD도 구할 수 있습니다.
이러한 방식은 CD/DVD로 데비안을 배포하는 완전 만족스러운 방법이죠.</p>

<p>물론, 이런 종류의 CD/DVD는 최종 사용자인 여러분에게도 유용합니다.
각 릴리스의 새로운 개정판 별로 CD/DVD 전체 세트를 굽는 것 대신,
여러분의 아키텍처용 업데이트 CD/DVD만 다운로드해서 구우면 됩니다.</p>

<p>업데이트 CD/DVD로 부팅할 수 없다는 것을 주의하세요. 업데이트 CD/DVD는
이미 설치된 것을 업그레이드하는데 필요한 패키지가 들어있습니다.
이미 설치된 것이 없다면, 일반 설치 CD/DVD가 필요합니다. 시스템이 부팅된 뒤에는
업데이트 CD/DVD를 <code>apt-cdrom add</code>로 추가할 수 있습니다.</p>

<p>자, 이미 이전 개정판의 CD/DVD/BD의 전체 세트를 가지고 있기는 하지만, 업데이트
CD/DVD를 다운로드하기 싫은 이유가 있다면 어떻게 할까요?
이런 경우는 <a href="../jigdo-cd/">jigdo</a>의 <q>업데이트</q> 기능을 고려해
볼만 합니다.
Jigdo는 오래된 CD/DVD/BD의 내용을 읽고, 새로운 CD/DVD에 변경된 파일만 다운로드
할 수 있습니다. 그리고 새로운 CD/DVD/BD의 전체 세트를 만들 수도 있죠.
여전히 업데이트 CD/DVD를 위한 동일한 데이터만 다운로드해서 처리합니다.</p>

# ============================================================

<toc-add-entry name="source-cd"><q>소스</q> CD는 뭔가요? </toc-add-entry>

<p>이미지 종류에는 두 가지가 있습니다. <q>바이너리</q> CD는 이미
컴파일되어 실행 가능한 프로그램이 들어 있습니다.
<q>소스</q> CD는 프로그램의 소스 코드가 들어 있습니다.
대다수의 사람들은 소스 CD가 필요치 않습니다. 여러분도 정말 합당한 이유가 있기
전에는 다운로드할 필요가 없죠.</p>

# ============================================================

<toc-add-entry name="nonfree">비자유(non-free)가 포함된 CD는 뭔가요?
</toc-add-entry>

<p>데비안은 소프트웨어 라이선스에 대해 꽤나 엄격합니다.
<a href="$(HOME)/social_contract#guidelines">데비안 자유 소프트웨어 가이드라인</a>의
관점에서 자유 소프트웨어만 실제 배포판에 허락됩니다.
모든 <q>비자유(non-free)</q> 소프트웨어, 예를 들면 소스코드가 없는 소프트웨어는
공식적으로 지원되지 않습니다.</p>

<p>지구상의 누구던, 어디에 있던 공식 CD를 자유롭게 활용할 수 있고 복사할 수
있으며 팔 수 있습니다. <q>비자유</q> 카테고리에 있는 패키지는 이와 충돌하기
때문에 제한되며 공식 CD에는 포함되지 않습니다.</p>

<p>가끔은 친절한 누군가가 비공식적으로 비자유 CD(non-free CD)를 굽기도 합니다.
우리 웹사이트에서 어떠한 링크도 못 찾았다면,
<a href="#mailing-list">데비안 CD 메일링 리스트</a>에 물어보기 바랍니다.</p>

# ============================================================

<toc-add-entry name="dvd">데비안 DVD 이미지도 있나요?</toc-add-entry>

<p>물론이죠. 데비안은 현재 안정(stable) 릴리스용 DVD 이미지를 제공합니다.
게다가, 우리가 아는 한, 데비안은 매주 전체 DVD 이미지를 제공하는 유일한 리눅스
배포판입니다!
이미지 크기 때문에 <a href="../jigdo-cd/">jigdo로 배포</a>되긴 하지만요.</p>

# ============================================================

<toc-add-entry name="official">공식과 비공식 이미지의 차이점은 뭔가요?
</toc-add-entry>

<p> 공식 이미지는 데비안 CD팀의 멤버가 만들고, 동작하는지 보장하는 테스트를
거칩니다. 그리고 한 번 릴리스되고 나면 이미지는 변하지 않습니다. 문제가 생긴
것이 밝혀지면, 다른 버전으로 새롭게 릴리스 되죠.</p>

<p>비공식 이미지는 누구나 만들 수 있습니다. CD팀 멤버, 다른 데비안 개발자나
숙련된 데비안 사용자처럼요. 전형적으로 비공식 이미지는 좀 더 최신이지만,
시험은 덜 거칩니다. 어떤 것들은 새로운 기능, 예를 들면 신규 하드웨어 지원 같은
것을 포함하거나 데비안 아카이브에 없는 추가적인 소프트웨어 패키지를 포함하기도
합니다.</p>

# ============================================================

<toc-add-entry name="live-cd"> 데비안 <q>라이브 CD</q>도 있나요?
</toc-add-entry>

<p>물론이죠. <q>라이브 CD</q>나 보다 정확히는 <q>라이브 시스템</q>이라고
하는데, DVD나 USB 메모리, 다른 매체 등을 위해 준비된 완전한 시스템입니다.
하드 디스크에 어떤 것도 설치할 필요가 없습니다. 대신 DVD나 USB 메모리 같은
매체로 부팅해서 머신으로 작업을 당장 시작할 수 있습니다.
모든 프로그램은 매체에서 직접 실행됩니다.</p>

<p><a href="$(HOME)/devel/debian-live/">데비안 라이브 프로젝트</a>가
다양한 시스템 타입과 매체용 <a href="../live/">라이브 이미지 파일</a>을
만듭니다.</p>

# ============================================================

<toc-add-entry name="bootable">CD/DVD/BD로 부팅이 실패했어요! / 어느 CD로
부팅을 해야 하나요?</toc-add-entry>

<p>첫 번째 CD/DVD/BD만 부팅 가능합니다.</p>

<p>부팅에 실패한다면 먼저 매체에 정확하게 구워졌는지 확인하세요.
<a href="#what-is">위에서 설명한 내용</a>을 보세요.
추가적으로 BIOS에서 광학 드라이브로 부팅하도록 설정했는지도 확인하세요.</p>

<p>CD/DVD/BD로 전혀 부팅이 안된다면,
<a href="$(HOME)/distrib/netinst#verysmall">USB 메모리나 네트워크</a>로도
부팅할 수 있습니다.</p>

# ============================================================

<toc-add-entry name="arch">M68K나 Hurd, 다른 아키텍처용 이미지는 어디에 있나요?
</toc-add-entry>

<p>특정 아키텍처 지원 상태에 따라 CD/DVD 이미지는 다른 곳에 있을 수 있습니다.</p>

<ul>

  <li>여러분이 찾는 아키텍처가 현재
  <a href="$(HOME)/releases/stable/">안정(stable) 릴리스</a>에서 공식적으로
  지원되면, 사용가능한 다운로드 옵션이 있는 <a href="../"><q>데비안 CD</q></a>
  페이지를 보세요.</li>

  <li>아키텍처용 데비안 포트가 있지만 아직 공식적으로 릴리스가 되지 않았다면,
  CD 이미지는 사용가능 할지 안 할지 모릅니다.
  다시 <a href="../"><q>데비안 CD</q></a>를 보세요. 안정(stable) 이미지와 달리
  다운로드 옵션이 딱 하나만 지원될 겁니다. 그러니 <q>jigdo</q>와 <q>HTTP</q>
  섹션을 보세요.</li>

  <li>GNU/Hurd용 데비안 포트는
  <a href="$(HOME)/ports/hurd/hurd-cd">비공식 Hurd CD 페이지</a>를 보세요.</li>

  <li>그 이외의 관심 있는 아키텍처는
  <a href="$(HOME)/ports/">데비안 포트 페이지</a>를 보세요.</li>

</ul>

# ============================================================

<toc-add-entry name="unstable-images"><q>불안정(unstable)</q> 배포판용 이미지가
있나요?</toc-add-entry>

<p><q>불안정(unstable)용</q> 전체 CD/DVD/BD 이미지는 없습니다.
<q>불안정(unstable)</q>에 있는 패키지가 너무 빨리 변한다는 사실 때문에, 사람들이
일반 데비안 HTTP 미러 서버를 사용해서 <q>불안정(unstable)</q>을 다운로드 하고
설치하거든요.</p>

<p><a href="../../releases/unstable/">불안정(unstable)을 실행하는 위험</a>을
인지하고 있는데도 여전히 설치하고 싶다면 몇 가지 선택지가 있습니다.</p>

<ul>

  <li><a href="../netinst/">네트워크 설치 이미지</a>를 사용해서
  <q>테스트(testing)</q>를 설치하세요. 그 후 <tt>/etc/apt/sources.list</tt>를
  수정해서 <q>불안정(unstable)</q>으로 업그레이드 하세요. 불필요한 다운로드와
  패키지 업그레이드를 피하려면, <q>테스트(testing)</q> 시스템을 최소한으로 먼저
  설치한 후 대부분의 패키지, 예를 들면 데스크톱 환경과 같은 것을
  <q>불안정(unstable)</q>으로 바꾼 후 설치하세요.</li>

  <li>안정(stable)용 설치 관리자로 <q>안정(stable)</q> 시스템을 최소로
      설치하세요. 그 후 <tt>/etc/apt/sources.list</tt> 파일을
      <q>테스트(testing)</q>로 변경한 다음 <tt>apt-get update</tt>와
      <tt>apt-get dist-upgrade</tt>를 실행하세요. 마지막으로 원하는 패키지를
      설치하세요. 이 방법은 여기 나열된 방법 중에서 제일 잘 동작합니다.</li>

  <li><q>테스트(testing)</q>용 설치 관리자의 테스터가 되어 netinst 이미지로
      <q>테스트(testing)</q>를 설치하세요. 그 후 <tt>/etc/apt/sources.list</tt>의
      항목을 수정하여 <q>불안정(unstable)</q>으로 업그레이드 하세요. 불필요한
      다운로드 및 패키지 업그레이드를 피하려면, <q>테스트(testing)</q> 시스템을
      최소한으로 먼저 설치한 후 대부분의 패키지, 예를 들면 데스크톱 환경과 같은
      것을 <q>불안정(unstable)</q>으로 바꾼 후 설치하세요. 그 후
      <tt>apt-get update</tt>와 <tt>apt-get -u dist-upgrade</tt>를 하면 sid
      릴리스가 됩니다.</li>

  <li>네트워크 부트용 <q>mini.iso</q> 이미지를 사용하세요.
      debian/dists/unstable/main/installer-*/current/images/netboot/mini.iso
      하위의 데비안 미러 서버에서 이미지를 찾을 수 있습니다. 설치 중에
      <q>고급 옵션</q> -> <q>전문가용 설치</q>를 선택하세요.
      <q>데비안 아카이브 미러 사이트 고르기</q> 단계에서
      <q>sid - 불안정(unstable)</q>을 선택하세요.</li>

</ul>

# ============================================================

<toc-add-entry name="search-for-package"> XYZ 패키지를 포함하고 있는 CD/DVD/BD
이미지는 어느 건가요?</toc-add-entry>

<p>어느 이미지가 어떤 파일을 포함하고 있는지 찾으려면, <a
href="https://cdimage-search.debian.org">CD 이미지 검색 도구</a>를 사용하세요.
이 도구는 데비안 우디(Woody) 릴리스 이후의 모든 데비안 CD/DVD/BD 정보를 갖고
있습니다. 오래되어 아카이브된 릴리스와 현재 안정(stable) 릴리스까지 모두 말이죠.
그리고 매일 및 매주의 테스트(testing) 빌드와 관련된 최신 세트도 가지고 있습니다.</p>

# ============================================================

<toc-add-entry name="list-image-contents">이미지에 저장된 모든 패키지의 리스트를
얻을 수 있나요?</toc-add-entry>

<p>물론입니다.
<a href="https://cdimage.debian.org/cdimage/">cdimage.debian.org</a>의
<tt>.list.gz</tt>를 보세요. 이 파일은 이미지에 포함된 모든 패키지와 소스 파일을
나열합니다.
데비안 라이브 이미지용은 이미지 파일과 같은 디렉터리에서 찾을 수 있는데,
<tt>.packages</tt>로 끝나는 비슷한 이름의 파일입니다.
이 파일을 다운로드한 후 원하는 패키지 이름을 검색하세요.</p>

# ============================================================

<toc-add-entry name="outdated">공식 CD의 소프트웨어가 오래되었습니다.
새로운 버전을 릴리스하는 건 어떤가요?</toc-add-entry>

<p>우리는 <q>안정(stable)</q> 배포판의 공식 릴리스를 정말로 만들만 하다고
생각이 들때만 만듭니다. 불행히도 안정(stable) 릴리스는 2년마다만 만들어진다는
뜻이죠...</p>

<p>데비안에 있는 몇몇 소프트웨어의 더 최신 버전을 원한다면,
<q>안정(stable)</q> 버전을 설치한 후 원하는 버전을 <q>테스트(testing)</q>에서
네트워크로 업그레이드 할 수 있습니다. 즉, 다른 릴리스에서 소프트웨어를 섞는 게
가능하다는 말입니다.</p>

<p>그 대신에, 매주마다 자동으로 생성되는 <q>테스트(testing)</q>의 이미지를
사용하세요. <a href="../../security/faq#testing"><q>테스트(testing)</q> 보안 지원</a>과
관련된 정보는 보안 FAQ에 있습니다.</p>

<p>특정 패키지의 새로운 버전만 필요하다면, 테스트(testing)에서 패키지를 가져와
안정(stable)에서 동작하게 만든 <q>백포트(backports)</q> 서비스를 이용할 수
있습니다. 이 옵션은 테스트(testing)에서 직접 똑같은 패키지를 설치하는 것보다
안전할 수 있죠.</p>

# ============================================================

<toc-add-entry name="newest">최신 이미지를 다운로드하고 있다는 걸 어떻게 알 수
있나요?</toc-add-entry>

<p><a href="../"><q>데비안 CD</q></a> 페이지의 아래에 있는 메모는 항상 최신
릴리스의 버전 정보를 보여줍니다.</p>

# ============================================================

<toc-add-entry name="verify">다운로드한 ISO 이미지와 구워진 광학 미디어를 어떻게
검증할 수 있나요?</toc-add-entry>

<p>서명된 체크섬 파일은 ISO 이미지 파일의 체크섬을 포함하고 있는데, 이를 어떻게
인증하는가에 대한 자세한 정보는 <a href="../verify">신뢰성 검증 페이지</a>에
있습니다. 암호 기법으로 체크섬 파일을 검증하고 나면, 우리는 다음 사항을 확인해
볼 수 있습니다.
</p>

<ul>
 <li><em>다운로드한 ISO 이미지 파일</em>의 체크섬이 체크섬 파일의 것과 일치합니다.
 ISO 이미지 체크섬 계산은 <q>sha512sum</q>와 <q>sha256sum</q> 도구를 이용합니다.
 </li>
 <li><em>이미 구워진 광학 미디어</em>의 체크섬이 체크섬 파일의 것과 일치합니다.
 이 방식은 설명하기에 다소 복잡한 작업입니다.</li>
</ul>

<p>구워진 광학 이미지를 검증할 때 문제는 어떤 매체 타입이 ISO 이미지보다
더 많은 바이트를 반환한다는 것입니다. 이처럼 뒤에 붙은 쓰레기 값은 TAO 모드로
구워진 CD, 추가적으로 기록된 DVD-R[W], 포맷된 DVD-RW, DVD+RW, BD-RE, USB
메모리에서는 피할 수 없습니다. 그러므로, 우리는 ISO 이미지 자체와 정확하게 같은
섹터 수만큼 읽는 것이 필요합니다. 미디어에서 더 읽는다면 체크섬 결과가
달라질테니까요.</p>

<ul>
 <li><q>isosize</q> 프로그램은 광학 미디어에서 몇 바이트를 읽어야 하는지 찾는데
  사용할 수 있습니다. 이 도구는 광학 미디어가 들어있는 <q>&lt;디바이스&gt;</q>
  파일의 <q>섹터 수(sector count)</q>와 <q>섹터 크기(sector size)</q>를
  보여줍니다.
  <br/><tt>$ /sbin/isosize -x &lt;디바이스&gt;</tt>
  <br/><tt>sector count: 25600, sector size: 2048
  </tt>
 </li>
 <li>그 후 <q>섹터 수</q>와 <q>섹터 크기</q>를 <q>dd</q>로 전달하여 광학 미디어에서
  적절한 바이트 수만큼 읽을 수 있도록 하고, 바이트 스트림을 체크섬 도구, 즉
  sha512sum 및 sha256sum 등으로 파이프를 통해 연결합니다.
  <br/><tt>$ dd if=&lt;디바이스&gt; count=&lt;섹터 수&gt; bs=&lt;섹터 크기&gt; | sha512sum
  </tt>
 </li>
 <li>계산된 체크섬을 적절한 체크섬 파일, 즉 SHA512SUMS, SHA256SUMS의 체크섬과
 비교할 수 있습니다.
 </li>
</ul>

<p>다른 대안으로, 유용한 도움을 주는 스크립트인 <a
href="https://people.debian.org/~danchev/debian-iso/check_debian_iso">
check_debian_iso</a>가 있습니다. 이는 <em>ISO 이미지 파일</em>과
<em>CD 미디어</em>를 검증할 수 있는데, 적절한 바이트 수만큼 미디어에서 읽은 후
체크섬을 계산하여 체크섬 파일과 비교합니다.</p>

<ul>
 <li><em>ISO 이미지 파일 검증</em>
  다음은 debian-6.0.3-amd64-netinst.iso 이미지 파일의 체크섬과
  SHA512SUMS 체크섬 파일의 관련된 체크섬을 비교합니다.
  <br/><tt>$ ./check_debian_iso  SHA512SUMS  debian-6.0.3-amd64-netinst.iso</tt>
 </li>
 <li><em>CD 미디어 검증</em>
  다음은 /dev/dvd로 접근 가능한 미디어의 체크섬과 SHA512SUMS에 있는
  debian-6.0.3-amd64-DVD-1.iso의 체크섬을 비교합니다.
  ISO 이미지 파일 자체는 필요 없고, 체크섬 파일의 관련된 체크섬을 찾기 위해
  이름만 필요하다는 것을 주의하세요.
  <br/><tt>$ ./check_debian_iso  SHA512SUMS  debian-6.0.3-amd64-DVD-1.iso  /dev/dvd</tt>
 </li>
</ul>

# ============================================================

<toc-add-entry name="small-dvd">다운로드 받은 DVD 이미지가 4&nbsp;GB보다
커야할 것 같은데 왜 1&nbsp;GB보다 작은가요?</toc-add-entry>

<p><a id="wget-dvd"></a> 십중팔구는 여러분이 다운로드할 때 쓴 도구가 대용량
파일을 지원하지 않기 때문입니다. 예를 들어, 4&nbsp;GBytes보다 큰 파일을
다운로드할 때 문제가 생기죠. 이러한 문제의 일반적인 현상은, 다운로드를 할 때
여러분이 사용한 도구가 알려준 다운로드한 데이터 크기가 4&nbsp;GB보다
많이 작다는 겁니다. 예를 들면, DVD 이미지 크기가 4.4&nbsp;GB라면 도구는
0.4&nbsp;GB라고 알려 줄겁니다.</p>

<p><tt>wget</tt>의 몇몇 오래된 버전도 이런 문제가 있습니다. 이러한 제약이 없는
버전으로 <tt>wget</tt>을 업그레이드하거나 다음과 같이 <tt>curl</tt> 명령행
다운로드 도구를 사용하세요.
<q><tt>curl -C - </tt><i>[URL]</i></q></p>

# ============================================================

<toc-add-entry name="record-unix">리눅스/유닉스 환경에서 ISO 이미지를 어떻게
굽나요?</toc-add-entry>

<p>데비안 ISO i386, amd64, arm64용 이미지는 USB 메모리로 부팅할 수 있습니다.
<a href="#write-usb">USB 메모리</a>를 보세요.</p>

<p>일반 유저도 사용할 수 있는 모든 광학 미디어 타입용
<a href="https://www.gnu.org/software/xorriso/xorriso.html">xorriso</a>는
다음과 같습니다.
<br/><tt>xorriso -as cdrecord -v dev=/dev/sr0 -eject debian-x.y.z-arch-MEDIUM-NN.iso</tt>
<br/> 명목상 최고 속도, 즉 드라이브의 내부 오류 관리로 발생하는 속도 저하 없이
BD-RE를 구우려면 <tt>stream_recording=on</tt> 옵션을 추가하세요.
</p>

<p>DVD와 BD 광학 미디어 타입용
<a href="http://fy.chalmers.se/~appro/linux/DVD+RW/">growisofs</a>는 다음과
같습니다.
<br/><tt>growisofs -dvd-compat -Z /dev/sr0=debian-x.y.z-arch-MEDIUM-NN.iso</tt>
</p>

<p>CD 광학 미디어 타입용 <a href="https://packages.debian.org/sid/wodim">wodim</a>은
다음과 같습니다.
<br/><tt>wodim -v dev=/dev/sr0 -eject -sao debian-x.y.z-arch-CD-NN.iso</tt>
</p>

<p>리눅스용 X 윈도우용 프로그램을 두세 가지 예를 들면
<a href="https://www.gnome.org/projects/brasero/">브라세로</a>,
<a href="http://k3b.plainblack.com/">K3B</a>,
<a href="http://www.xcdroast.org/">X-CD-Roast</a>가 있습니다.
이러한 프로그램은 앞서 설명한 로우레벨 이미지 굽기 프로그램의 프론트엔드임을
유의하세요.</p>

<dl>
  <dt><strong><a id="brasero"
  href="https://www.gnome.org/projects/brasero/">브라세로</a></strong></dt>

  <dd><i>이미지 굽기</i>를 선택하세요.
  <i>여기를 눌러 디스크 이미지 선택</i>을 누르고 다운로드한 ISO 파일을
  선택하세요. <i>속성</i>의 설정이 올바른지 확인한 후 <i>이미지 만들기</i>를
  선택하세요.</dd>

  <dt><strong><a id="k3b"
  href="http://k3b.plainblack.com/">K3b</a></strong></dt>

  <dd>메뉴 항목의 <i>도구 - CD - CD 이미지 굽기</i>를 선택하세요.
  대화상자가 열리면 <i>기록할 이미지 파일</i> 항목에 이미지 경로를
  입력하고, 다른 설정들이 올바른지 확인한 후 <i>시작</i>을 누르세요.
  </dd>

  <dt><strong><a id="xcdroast"
  href="http://www.xcdroast.org/">X-CD-Roast</a></strong></dt>

  <dd>프로그램이 시작되면, <i>설정(Setup)</i>을 눌러서 <i>HD 설정(HD settings)</i>
  탭을 선택하세요. 데비안 CD 이미지를 테이블에 표시된 디렉터리 중 하나에 복사하세요.
  만약 테이블이 비어있다면, 임시 저장소로 사용할 디렉터리 경로를 입력한 후
  <i>추가(Add)</i>를 누르세요. 설정에서 나가려면 <i>확인(OK)</i>을 누르세요.
  다음은 <i>CD 생성(Create CD)</i>을 선택한 후 <i>트랙 쓰기(Write Tracks)</i>를
  선택하세요. <i>트랙 레이아웃(Layout tracks)</i> 탭을 고른 후, 표시된 이미지
  파일 이름을 선택하고 <i>추가(Add)</i>를 누르세요. 그후
  <i>트랙 레이아웃 수락(Accept track layout)</i>을 누르세요.
  끝으로 <i>트랙 쓰기(Write tracks)</i>를 누르면 됩니다.</dd>

</dl>

# ============================================================

<toc-add-entry name="record-windows">윈도우에서 ISO 이미지를 어떻게 굽나요?
</toc-add-entry>

<p>오래된 윈도우 버전에서는 조금 문제가 있을지도 모릅니다. 왜냐하면 수많은
윈도우용 이미지 굽기 프로그램이 자체 CD 이미지 형식을 사용하거든요.
<tt>.iso</tt> 이미지를 구우려면, <q>특별한</q> 매뉴를 사용해야할 겁니다.
<q>ISO9660 파일</q>이나 <q>가공하지 않은(Raw) ISO 이미지</q>,
<q>2048 바이트/섹터</q>와 같은 옵션을 찾으세요. 다른 바이트/섹터 값은 치명적일
수 있으니 주의하세요. 몇몇 프로그램은 이런 선택지가 없을 수 있으니, 다른 CD 굽기
프로그램을 사용하고 친구나 동료에게 물어보세요. 특정 제품을 이용해서 CD 이미지를
어떻게 굽는지는 다음을 보세요.</p>

<dl>

  <dt><strong><a name="imgburn"
  href="http://www.imgburn.com/">ImgBurn</a></strong>
  (Freeware)</dt>
  
  <dd>CD/DVD에 이미지를 굽는 방법과 관련된
  <a href="http://www.imgburn.com/index.php?act=screenshots#isowrite">스크린샷</a>이
  있습니다.</dd>

  <dt><strong><a name="cdburnerxp"
  href="https://cdburnerxp.se/">CDBurnerXP Pro</a></strong>
  (프리웨어)</dt>

  <dd><tt>.iso</tt> 이미지를 굽는 과정은
  <a href="https://cdburnerxp.se">프로그램 매뉴얼</a>에 있습니다.</dd>

  <dt><strong><a name="isorecorder"
  href="http://isorecorder.alexfeinman.com/">ISO
  Recorder</a></strong> (프리웨어)</dt>

  <dd>이 프로그램은 <tt>.iso</tt> 이미지를 윈도우 2003, XP, 비스타(Vista)에서
  구울 수 있습니다.</dd>

  <dt><strong><a NAME="adaptec" href="http://www.adaptec.com/">Adaptec</a>\
  /<a href="https://www.roxio.com/">Roxio</a> Easy-CD Creator</strong></dt>

  <dd><i>파일(File)</i> 매뉴에서 <i>이미지로 CD 생성(Create CD from image...)</i>을
  선택합니다. 그 후 <q>.iso</q> 파일 타입과 정확한 이미지를 선택합니다. 이는
  CD 굽기 설정 GUI를 표시하는데, 여기에 나온 여러분의 CD-R 관련 정보가 모두
  정확한지 확인하세요. <i>생성 옵션(Create options)</i>에서는
  <i>CD 생성(Create CD)</i>을 선택하세요. 아래에
  <i>쓰기 방법(Write method)</i>에서는 <i>트랙 한 번에 쓰기(Track at once)</i>와
  <i>CD 닫기(Close CD)</i>를 선택하세요.</dd>

  <dt><strong><a href="http://www.ahead.de/">Ahead Software</a></strong>의
  <a NAME="nero">Nero</a>
  </dt>

  <dd>위저드를 비활성화한 후 <q>파일(File)</q>이나 <q>레코드(Recorder)</q> 매뉴에서
  <i>이미지 굽기(Burn Image)</i>를 선택하세요. 필요하면 파일 선택 윈도우에서
  <i>모든 파일(All Files)</i>을 선택하세요. <tt>.iso</tt> 파일을 선택하고
  <q>외부 파일입니다(this is a foreign file)</q> 대화상자의 OK를 누르세요.
  옵션 박스가 표시되면 기본 설정으로 두는 것이 좋습니다.
  <q><i>데이터 모드(Data Mode) 1</i></q>, <q><i>블럭 크기(Block Size) 2048</i></q>,
  <q><i>로우 데이터, 스크램블, 스왑(Raw Data, Scrambled, and Swapped)</i></q>
  <strong>선택 해제</strong>, 그리고 <i>이미지 헤더(Image Header)</i>와
  <i>이미지 트레일(Image Trailer)</i>은 0으로 두세요.
  OK를 누르고 아래의 <i>CD 쓰기(Write CD)</i>나 <i>굽기(Burn)</i>는
  기본 설정, 예를 들어 <i>쓰기(Write)</i>와
  <i>최대 속도 측정(Determine maximum speed)</i>을 사용하세요. 추가로
  <i>CD 마무리(Finalize CD)</i> 옵션도 체크하세요.</dd>

  <dt><strong><a NAME="resource-kit-tools"
  href="https://www.microsoft.com/en-us/download/details.aspx?id=17657">마이크로소프트
  리소스 킷 도구</a></strong></dt>

  <dd>마이크로소프트는 명령행 리소스 킷 도구를 무료로 배포하며, 윈도우 2003과
  XP에서 동작합니다. 이미지를 CD나 DVD에 굽는 두 프로그램이 포함되어 있는데,
  이름은 <tt>Cdburn.exe</tt>과 <tt>Dvdburn.exe</tt>입니다. 이 프로그램의 사용법은
  동봉된 도움말 파일에 설명되어 있으며,
  <tt>cdburn&nbsp;drive:&nbsp;iso-file.iso&nbsp;/speed&nbsp;max</tt>와 같은
  방식으로 명령어를 실행할 수 있습니다.</dd>

</dl>

<p>갱신된 정보나 다른 프로그램에 관한 내용을 제공하고 싶다면, <a
href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;debian-cd&#64;lists.debian.org">로
보내 알려주세요.</a>.

# ============================================================

<toc-add-entry name="record-mac">맥 OS에서 ISO 이미지를 어떻게 굽나요?
</toc-add-entry>

<p>맥 OS용 <strong>Toast</strong> 프로그램이 <tt>.iso</tt> 파일과 잘 맞다고
알려져 있습니다. FileTyper 같은 프로그램으로 <i>CDr3</i>나 가능하면 <i>CDr4</i>
같은 생성 코드와 <i>iImg</i> 같은 타입 코드를 넣어줌으로써 더욱 안전하게 사용할
수도 있습니다. 드래그 앤 드롭이나 파일 열기 매뉴를 통하지 않아도 파일을 빠르게
두 번 누르면 Toast로 직접 열리거든요.</p>

<p>다른 옵션으로는 맥 OS&nbsp;10.3 이상에 들어있는 <strong>Disk Utility</strong>가
있습니다. <tt>/Applications/Utilities</tt> 폴더의 Disk Utility 프로그램을 열고
<i>이미지(Image)</i>매뉴의 <i>굽기(Burn)...</i>를 선택한 후 구울 CD 이미지를
고르세요. 모든 설정이 올바르다고 확신하면 <i>굽기(Burn)</i>를 누르세요.</p>


# ============================================================

<toc-add-entry name="write-usb">CD/DVD/BD 이미지를 USB 메모리에 어떻게 쓰나요?
</toc-add-entry>

<p>몇몇 데비안과 데비안 라이브 이미지, 특히 모든 i386, amd64, arm64 이미지들은
<i>isohybrid</i> 기술을 사용하여 만들어집니다. 이는 두 가지 다른 방법으로
사용될 수 있다는 것을 의미합니다.</p>

<ul>
  <li>이미지는 CD/DVD/BD에 써진 뒤 일반 CD/DVD/BD 부팅에 사용될 수 있습니다.</li>
  <li>이미지는 USB 메모리에 써진 뒤 대부분 PC의 BIOS/EFI 펌웨어로 바로 부팅될
  수 있습니다.</li>
</ul>

<p>리눅스 컴퓨터에서는 단순히 <q>cp</q> 명령을 써서 USB 메모리에 이미지를
복사합니다.</p>

<p><code>cp &lt;file&gt; &lt;device&gt;</code></p>

<p>또 다른 방법으로 <q>dd</q>를 써도 됩니다.</p>

<p><code>dd if=&lt;파일&gt; of=&lt;디바이스&gt; bs=4M; sync</code></p>

<p>여기서</p>
<ul>
  <li>&lt;파일&gt;은 입력 이미지 이름, 예를들어 <q>netinst.iso</q> 같은 것입니다.
  <li>&lt;디바이스&gt;는 USB 메모리에 해당하는 장치,
  예를 들어 /dev/sda, /dev/sdb 같은 것입니다. <em>정확한 디바이스 이름인지
  주의 깊게 확인</em>하세요. 이 명령은 잘못된 이름을 쓰면 간단히 여러분의
  하드 디스크를 덮어쓸 수 있습니다.</li>
  <li><q>bs=4M</q>는 더 나은 성능을 위해 4메가바이트 덩어리로 읽고 씁니다.
  기본값은 512 바이트인데 많이 느릴 겁니다.
  <li><q>sync</q>는 명령어가 반환되기 전에 모든 쓰기가 완료되는 것을 보장합니다.
</ul>

<p>위의 리눅스 시스템용 방법에 덧붙이자면, 윈도우에서 부팅 가능한 USB 메모리를
   쓸 수 있게 해주는 <a NAME="win32diskimager"
   href="https://sourceforge.net/projects/win32diskimager/">win32diskimager</a>가
   있습니다. <b>힌트:</b>win32diskimager는 기본적으로 이름이 정확히
   <i>*.img</i>인 파일만 표시해주는 반면, 데비안 이미지는 이름이
   <i>*.iso</i>입니다. 이 도구를 사용할 때 필터를 <i>*.*</i>로 바꾸세요.
</p>

<p>데비안은 이와 같은 작업에 대해 <q>unetbootin</q>을 사용하지 않도록 권장한다는
것에 주의하세요. 부팅 및 설치 시 진단하기 어려운 문제가 발생할 수 있으므로
권장하지 않습니다.
</p>

# ============================================================

<toc-add-entry name="whatlabel">디스크에 레이블을 어떻게 달아야 하나요?
</toc-add-entry>

<p>레이블을 다는데 의무적인 것은 없습니다. 다만, 호환성을 보장하기 위해 아래
체계를 따르길 권합니다.</p>

<div class="cdflash">
      <p>Debian GNU/{Linux|Hurd|kFreeBSD}
      &lt;버전&gt;[&lt;리비전&gt;]<br />Official
      {&lt;아키텍처&gt;} {CD|DVD|BD}-&lt;번호&gt;</p>
</div>

<p>예를 들면 아래와 같습니다.</p>

<div class="cdflash">
      <p>Debian GNU/Linux 6.0.3<br />Official i386
      CD-1</p>
</div>

<div class="cdflash">
      <p>Debian GNU/Linux 6.0.3<br />Official
      amd64 DVD-2</p>
</div>

<div class="cdflash">
      <p>Debian GNU/Linux 6.0.3<br />Official
      source BD-1</p>
</div>

<div class="cdflash">
      <p>Debian GNU/kFreeBSD 6.0.3<br />Official i386
      Netinst CD</p>
</div>

<p>공간이 충분하다면 첫 번째 라인에 코드네임(codename)을 추가할 수도 있습니다.
<i>Debian GNU/Linux 6.0.3 <q>Squeeze</q></i>처럼 말이죠.</p>

<p><em>Official</em>이란 명칭은 여러분의 CD 이미지가
<a href="../jigdo-cd/#which">공식 릴리스의 jigdo 파일</a> 중 하나와 체크섬이
<strong>일치할 때만</strong> 사용가능하니 주의하세요.
여러분이 직접 만들었거나 체크섬이 일치하지 않는 모든 CD는 명확히
<em>Unofficial</em>로 레이블링 되어야 합니다. 예를 들면 아래처럼 말이죠.

<div class="cdflash">
      <p>Debian GNU/Linux 6.0.3<br />Unofficial
      Non-free</p>
</div>

<p>공식 위클리 스냅샷의 경우는 데비안 버전으로 릴리스된 것과 혼란을 피하기 위해
<q>6.0.3</q>와 같은 버전명을 쓰지 않는 것이 좋습니다. 그 대신 <q>에치(etch)</q>와
같은 코드네임이나 <q>테스트(testing)</q>와 같은 배포판 이름을 같이 레이블링
하세요. 구분하는데 도움을 주기위해 <q>스냅샷(Snapshot)</q>과 스냅샷의 날짜를
추가할 수도 있습니다.</p>

<div class="cdflash">
      <p>Debian GNU/Linux <q>etch</q><br />
      Official Snapshot alpha Binary-2<br />
      2005-06-17</p>
</div>

# ============================================================

<toc-add-entry name="artwork">디스크와 커버를 위한 삽화가 있나요?
</toc-add-entry>

<p>데비안 CD/DVD/BD용 공식 커버와 뒷면, 레이블 레이아웃은 없습니다. 하지만,
많은 사람들이 멋진 이미지를 만들었죠. <a href="../artwork/">삽화 페이지</a>를
살펴보세요.

# ============================================================

<toc-add-entry name="old">오래된 CD/DVD/BD 이미지가 아직 있나요?
</toc-add-entry>

<p>몇몇 오래된 이미지는 <a
href="https://cdimage.debian.org/cdimage/archive/">cdimage.debian.org의
아카이브 섹션</a>에 있습니다. 예를 들어, 이미 새 릴리스가 나왔지만 어떤
아키텍처의 지원이 필요해서 오래된 이미지를 써보고 싶을지도 모르니까요.</p>

<p>4.0 에치(Etch) 이전의 정말 오래된 CD/DVD를 설치했을 때,
<tt>/etc/apt/sources.list</tt>에는 <em>현재</em> 안정(stable) 데비안 릴리스가
기본적으로 들어 있을 겁니다. 이는 인터넷을 통한 모든 업그레이드는 현재
안정(stable) 릴리스로 된다는 걸 뜻합니다.</p>

# ============================================================

<toc-add-entry name="lan-install">서로 연결된 수많은 컴퓨터에 데비안을 설치하는
가장 좋은 방법은 뭔가요?</toc-add-entry>

<p>수많은 머신에 데비안을 설치하고 싶고 설치된 모든 것을 최신 상태, 예를 들어
보안 업데이트 같은 것을 유지하고 싶다면, 광학 미디어로 설치하는 것은 좋은 생각이
아닙니다. 인터넷으로 설치하는 것도 좋은 생각이 아니죠. 왜냐면 각 머신에서
패키지를 또 다운로드 할 거거든요. 이런 경우는 다음 세 가지 옵션으로 로컬 캐시를
구축하는 것이 좋습니다.</p>

<ul>

  <li><em>디스크의 내용을 HTTP로 사용가능하게 만들기:</em> 이미지를 다운로드한
  후, 로컬 미러 서버의 LAN으로 사용가능하게 만드세요. 각 머신은 이 미러 서버를
  정규 데비안 서버처럼 이용할 수 있습니다. 예를 들어, CD의 내용이
  <tt>http://10.0.0.1/cd1/</tt> URL로 접근 가능하다면, 로컬 네트워크의 머신은
  <tt>/etc/apt/sources.list</tt>에 아래 라인을 추가함으로써 패키지를 이용할 수
  있습니다.<br />
  <tt>deb http://10.0.0.1/cd1/ stable main contrib</tt><br />
  CD 각각을 위해서는 개별 URL과 항목이<tt>sources.list</tt>에 필요합니다.
  </li>

  <li><em>.deb 파일 캐시를 위해 HTTP 프록시 설정하기:</em>
  프록시가 .deb 파일을 오랫동안 보관하도록 설정하세요. 그 후 각 머신의
  <tt>http_proxy</tt> 환경이 캐시를 가리키도록 설정하고, apt의 HTTP 인수 기법을
  사용하세요.<br />
  이 방법은 관리자를 번거롭게 하지 않으면서 운영 중인 미러 서버의 장점을 최대한
  활용합니다. 프록시의 디스크 사용량을 제한할 수 있기 때문에, 디스크 공간이
  제약된 사이트에서도 동작이 가능합니다. 그리고 여러분이 설치할 패키지만 다운로드
  하기 때문에 미러링을 하는 것보다 유리하고 대역폭도 절약됩니다.
  Squid는 다음 라인을 <tt>/etc/squid/squid.conf</tt>에 추가함으로써 파일을
  보관하도록 알려줄 수 있습니다.<br />
  <tt>refresh_pattern&nbsp;&nbsp;&nbsp;debian.org/.*.deb$&nbsp;&nbsp;&nbsp;129600&nbsp;100%&nbsp;129600</tt></li>

  <li><em>사설 데비안 패키지 미러 서버를 구축하기:</em>
  데비안 아카이브는 엄청나게 커지고 있다는 걸 알아두세요! 자세한 내용은
  <a href="$(HOME)/mirror/">미러링 페이지</a>를 참조하세요.</li>

</ul>

<p>수많은 머신에 설치하는 건 어려운 일일 수 있습니다. 이 작업을 도와줄 수 있는
<a href="https://fai-project.org/">완전 자동 설치</a> (fully automatic installation,
FAI)용 데비안 패키지도 있습니다.</p>

# ============================================================

<toc-add-entry name="diy">로컬 데비안 미러 서버가 있는데 CD/DVD/BD를 만들고
싶습니다. 어떻게 해야 하나요?</toc-add-entry>

<p><a href="$(HOME)/mirror/">로컬 데비안 미러 서버</a>와는 별개로, 엄청난 디스크
공간도 필요합니다. 이미지 생성 스크립트는 <em>debian-cd</em> 패키지에 묶여
있습니다. git에 있는 최신 코드를 쓰는 게 일반적으로 더 좋지만, 여전히
필요한 도구를 다 갖고 있는지 패키지의 의존성을 살펴봐야 합니다.</p>

<p>git에 있는 최신 버전을 받으려면 git 설치되어 있어야 합니다. 빈 디렉터리에서
아래 명령을 입력하세요.</p>

<div class="centerblock">
<p>
<tt>git clone https://salsa.debian.org/images-team/debian-cd.git</tt>
</p>
</div>

<p>스크립트를 사용해야만 하나요? 그렇다면 불가피하게 나오는 질문의 해답을 찾기
위해 <a href="https://lists.debian.org/debian-cd/">debian-cd 메일링 리스트 아카이브</a>를
확인해보세요. :-)</p>

# ============================================================

<toc-add-entry name="become-cd-mirror"> 데비안 CD/DVD/BD 이미지용 미러 서버가
되려면 어떻게 해야 하나요?</toc-add-entry>

<p>debian-cd 이미지 미러 서버를 설정하고 최신으로 유지하는데 필요한 단계는
<a href="../mirroring/">별도의 페이지에 설명되어</a> 있습니다.</p>

# ============================================================

<toc-add-entry name="not-all-images">몇몇 이미지가 없습니다! 처음부터 몇 개만
사용 가능합니다. 나머지는 어디에 있나요?</toc-add-entry>

<p>우리는 모든 아키텍처의 ISO 이미지 전체 세트를 저장하거나 제공하지 않습니다.
미러 서버의 공간을 줄이기 위해서죠.
그 대신, <a href="#why-jigdo">jigdo 도구를 사용해서</a> 사라진 ISO 이미지를
다시 만들 수 있습니다.</p>

# ============================================================
