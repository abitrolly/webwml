#use wml::debian::cdimage title="BitTorrent로 데비안 CD 이미지 다운로드" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="e8bc071549727e1ed1d215013db8ccabd37bb3e0" maintainer="Sebul"

<p><a href="https://en.wikipedia.org/wiki/BitTorrent">BitTorrent</a>는
피어 투 피어 다운로드 시스템이며 많은 다운로더에 최적화되어있습니다.
BitTorrent 클라이언트는 다운로드하는 동안 파일 조각을 다른 사람에게 업로드하여 네트워크 전체에 부하를 분산시키고
빠른 다운로드를 가능하게하기 때문에 서버 부하를 분산합니다.
</p>
<div class="tip">
<p><strong>첫</strong> CD/DVD 디스크에는 표준 데비안 시스템을 설치하는데 필요한 모든 파일이 있습니다.<br />
불필요한 다운로드를 피하려면 패키지가 필요하다는 것을 모르면 다른 CD 또는 DVD 이미지 파일을 다운로드 하지 <strong>마세요</strong>.
</p>
</div>
<p>
이런 방식으로 데비안 CD/DVD 이미지를 다운로드하려면 BitTorrent 클라이언트가 필요합니다.
데비안 배포본에는 <a href="https://packages.debian.org/bittornado">BitTornado</a>,
<a href="https://packages.debian.org/ktorrent">KTorrent</a> 및 원래 
<a href="https://packages.debian.org/bittorrent">BitTorrent</a> 도구가 들어 있습니다.
다른 운영 체제는 <a
href="http://www.bittornado.com/download.html">BitTornado</a> 및 <a
href="https://www.bittorrent.com/download">BitTorrent</a>에서 지원됩니다.
</p>
<h3><q>안정</q> 릴리스용 공식 토런트</h3>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>문서를 본 다음 설치하세요.
<strong>단 하나의 문서만</strong> 설치 전에 읽는다면, 우리의
<a href="$(HOME)/releases/stable/i386/apa">설치 Howto</a>, 간단한 설치 절차를 읽으세요. 다른 쓸만한 문서:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">설치 안내</a>,
    자세한 설치 명령</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer
    문서</a>, FAQ 포함 공통 문답</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    정오표</a>, 설치관리자 알려진 문제 목록</li>
</ul>

# <h3>Official torrents for the <q>testing</q> distribution</h3>
# 
# <ul>
# 
#   <li><strong>CD</strong>:<br />
#   <full-cd-torrent>
#   </li>
# 
#   <li><strong>DVD</strong>:<br />
#   <full-dvd-torrent>
#   </li>
# 
# </ul>

<p>
가능하면 다운로드가 완료된 후 다른 사용자가 이미지를 더 빨리 다운로드 할 수 있도록 클라이언트를 떠나십시오!
</p>
