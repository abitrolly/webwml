#use wml::debian::translation-check translation="70be9894f7f65be4520c817dfd389d2ee7c87f04" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В git, быстрой масштабируемой распределённой системе управления изменениями,
было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1348">CVE-2019-1348</a>

    <p>Было сообщено, что опция --export-marks для git fast-import
    открыта через поточную командную возможность export-marks=...,
    что позволяет произвольно переписывать пути.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1387">CVE-2019-1387</a>

    <p>Было обнаружено, что имена подмодулей недостаточно строго проверяются,
    что позволяет выполнять направленные атаки через удалённое выполнение кода
    при выполнении рекурсивного клонирования.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19604">CVE-2019-19604</a>

    <p>Йорн Шнивайц сообщил об уязвимости, при которой рекурсивное клонирование,
    за которым следует обновление подмодуля, может выполнять код, содержащийся в
    репозитории, без явного запроса об этом у пользователя. Теперь эта возможность
    отключена, в `.gitmodules` записи вида `submodule.&lt;name&gt;.update=!command`
    выключены.</p></li>

</ul>

<p>Кроме того, данное обновление исправляет ряд проблем безопасности, которые
представляют опасность только в том случае, когда git работает в файловой системе NTFS (<a href="https://security-tracker.debian.org/tracker/CVE-2019-1349">CVE-2019-1349</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a> и <a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>).</p>

<p>В предыдущем стабильном выпуске (stretch) эти проблемы были исправлены
в версии 1:2.11.0-3+deb9u5.</p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 1:2.20.1-2+deb10u1.</p>

<p>Рекомендуется обновить пакеты git.</p>

<p>С подробным статусом поддержки безопасности git можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4581.data"
