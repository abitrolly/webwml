#use wml::debian::translation-check translation="bbdc5fdd9cf31c8e006e51f38f90961f3cb78f42" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i Subversion, et versionsstyringssystem.  
Projektet Common Vulnerabilities and Exposures har registreret følgende 
problemer:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11782">CVE-2018-11782</a>

    <p>Ace Olszowka rapporterede at Subversions svnserve-serverproces kunne 
    afslutte, når en veldannet readonly-forespørgsel medførte et bestemt 
    svar, førende til et lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0203">CVE-2019-0203</a>

    <p>Tomas Bortoli rapporterede at Subversions svnserve-serverproces kunne 
    afslutte, når en klient sender visse sekvenser af protokolkommandoer.  Hvis 
    serveren er opsat med aktiveret anonym adgang, kunne det føre til et fjernt 
    uautentificeret lammelsesangreb.</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 1.9.5-1+deb9u4.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 1.10.4-1+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine subversion-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende subversion, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/subversion">\
https://security-tracker.debian.org/tracker/subversion</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4490.data"
