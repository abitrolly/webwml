msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/devel/debian-installer/ports-status.defs:10
msgid "Working"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:20
msgid "sarge"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:30
msgid "sarge (broken)"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:40
msgid "Booting"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:50
msgid "Building"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:56
msgid "Not yet"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:59
msgid "No kernel"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:62
msgid "No images"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:65
msgid "<void id=\"d-i\" />Unknown"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:68
msgid "Unavailable"
msgstr ""

#: ../../english/devel/website/tc.data:11
msgid "See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> (only available in French) for more information."
msgstr ""

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr ""

#: ../../english/devel/website/tc.data:13
msgid "See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</a> (only available in Spanish) for more information."
msgstr ""

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr ""

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr ""

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr ""

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr ""

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr ""

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr ""

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr ""

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr ""

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr ""

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr ""

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr ""

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr ""

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr ""

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr ""

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr ""

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr ""

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr ""

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr ""

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr ""

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr ""

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr ""

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr ""

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr ""

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr ""

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr ""

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr ""

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr ""

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr ""

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr ""

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr ""

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr ""

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr ""

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr ""

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr ""

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:36
msgid "Do you have any tips for women interested in getting more involved with Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:39
msgid "Are you involved with any other women in technology group? Which one(s)?"
msgstr ""

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr ""

