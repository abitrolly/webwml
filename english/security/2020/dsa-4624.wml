<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in evince, a simple multi-page
document viewer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000159">CVE-2017-1000159</a>

    <p>Tobias Mueller reported that the DVI exporter in evince is
    susceptible to a command injection vulnerability via specially
    crafted filenames.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>

    <p>Andy Nguyen reported that the tiff_document_render() and
    tiff_document_get_thumbnail() functions in the TIFF document backend
    did not handle errors from TIFFReadRGBAImageOriented(), leading to
    disclosure of uninitialized memory when processing TIFF image files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010006">CVE-2019-1010006</a>

    <p>A buffer overflow vulnerability in the tiff backend could lead to
    denial of service, or potentially the execution of arbitrary code if
    a specially crafted PDF file is opened.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 3.22.1-3+deb9u2.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 3.30.2-3+deb10u1. The stable distribution is only affected by
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>.</p>

<p>We recommend that you upgrade your evince packages.</p>

<p>For the detailed security status of evince please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/evince">https://security-tracker.debian.org/tracker/evince</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4624.data"
# $Id: $
