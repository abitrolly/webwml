<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a double-free vulnerability in BlueZ, a
suite of Bluetooth tools, utilities and daemons.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27153">CVE-2020-27153</a>

    <p>In BlueZ before 5.55, a double free was found in the gatttool
    disconnect_cb() routine from shared/att.c. A remote attacker could
    potentially cause a denial of service or code execution, during service
    discovery, due to a redundant disconnect MGMT event.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
5.43-2+deb9u3.</p>

<p>We recommend that you upgrade your bluez packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2410.data"
# $Id: $
