<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was found in pam_radius: the password length check
was done incorrectly in the add_password() function in pam_radius_auth.c,
resulting in a stack based buffer overflow.</p>

<p>This could be used to crash (DoS) an application using the PAM stack
for authentication.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.16-4.4+deb8u1.</p>

<p>We recommend that you upgrade your libpam-radius-auth packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2116.data"
# $Id: $
