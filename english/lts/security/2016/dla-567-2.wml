<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the MySQL Database Server. These
vulnerabilities are addressed by upgrading MySQL to the recent upstream 5.5.50
version. Please refer to the MySQL 5.5 Release Notes and Oracle's Critical
Patch Update advisory for further details:</p>

<ul>
<li><url "https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-50.html"></li>
<li><a href="http://www.oracle.com/technetwork/security-advisory/cpujul2016-2881720.html">http://www.oracle.com/technetwork/security-advisory/cpujul2016-2881720.html</a></li>
</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.5.50-0+deb7u2.</p>

<p>Regression note: I have wrongly built the previous 5.5.50-0+deb7u1 upload over
the jessie-security debian packaging. Although I have not identified any issues
on amd64, I have uploaded a new release built on the regular wheezy packaging.</p>

<p>We recommend that you upgrade your mysql-5.5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-567-2.data"
# $Id: $
