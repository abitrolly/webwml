<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Three security issues have been found in cacti:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2313">CVE-2016-2313</a>

    <p>auth_login.php allows remote authenticated users who use web
    authentication to bypass intended access restrictions by logging in
    as a user not in the cacti database.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3172">CVE-2016-3172</a>

    <p>An SQL injection vulnerability in tree.php allows remote authenticated
    users to execute arbitrary SQL commands via the parent_id parameter in
    an item_edit action.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3659">CVE-2016-3659</a>

    <p>An SQL injection vulnerability in graph_view.php allows remote
    authenticated users to execute arbitrary SQL commands via the
    host_group_data parameter.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.8.8a+dfsg-5+deb7u9.</p>

<p>We recommend that you upgrade your cacti packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-560.data"
# $Id: $
