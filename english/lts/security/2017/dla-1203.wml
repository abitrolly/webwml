<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The scp_v0s_accept function in sesman/libscp/libscp_v0.c in the session manager
in xrdp uses an untrusted integer as a write length, which allows local users
to cause a denial of service (buffer overflow and application crash) or
possibly have unspecified other impact via a crafted input stream.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.5.0-2+deb7u2.</p>

<p>We recommend that you upgrade your xrdp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1203.data"
# $Id: $
