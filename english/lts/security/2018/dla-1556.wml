<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000805">CVE-2018-1000805</a>

      <p>Fix to prevent malicious clients to trick the Paramiko server into
      thinking an unauthenticated client is authenticated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7750">CVE-2018-7750</a>

      <p>Fix check whether authentication is completed before processing
      other requests. A customized SSH client can simply skip the
      authentication step.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.15.1-1+deb8u1.</p>

<p>We recommend that you upgrade your paramiko packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1556.data"
# $Id: $
