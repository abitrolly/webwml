<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the set_text_distance function in
base/gdevpdts.c in the pdfwrite component in Ghostscript does not
prevent overflows in text-positioning calculation, which allows remote
attackers to cause a denial of service (application crash) or possibly
have unspecified other impact via a crafted PDF document.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.05~dfsg-6.3+deb7u8.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1363.data"
# $Id: $
