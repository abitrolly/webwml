#use wml::debian::template title="Debian 11 -- Σημειώσεις έκδοσης" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="ce7e85638a047b969574a7abeb8a634c27b9086b" maintainer="galaxico"

<if-stable-release release="stretch">
<p>Η παρούσα είναι μια <strong>υπό ανάπτυξη έκδοση</strong> των Σημειώσεων της έκδοσης για το Debian 10, με κωδική ονομασία buster, που δεν έχει κυκλοφορήσει ακόμα. Οι πληροφορίες που παρουσιάζονται εδώ μπορεί να μην είναι ακριβείς ή να είναι παρωχημένες και το πιο πιθανόν ελλιπείς.</p>
</if-stable-release>

<if-stable-release release="buster">
<p>Η παρούσα είναι μια <strong>υπό ανάπτυξη έκδοση</strong> των Σημειώσεων της έκδοσης για το Debian 11, με την κωδική ονομασία bullseye, που δεν έχει κυκλοφορήσει ακόμα. Οι πληροφορίες που παρουσιάζονται εδώ μπορεί να μην είναι ακριβείς ή να είναι παρωχημένες και το πιο πιθανόν ελλιπείς.</p>
</if-stable-release>

<p>Για να βρείτε τι καινούριο υπάρχει στην έκδοση Debian 11, δείτε τις Σημειώσεις της έκδοσης για την αρχιτεκτονική σας:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Release Notes'); :>
</ul>

<p>Οι Σημειώσεις της έκδοσης περιέχουν οδηγίες για τους χρήστες που κάνουν μια αναβάθμιση από παλιότερες εκδόσεις.</p>

<p>Αν έχετε ρυθμίσεις σωστά την τοπικοποίηση του περιηγητή σας, μπορείτε να χρησιμοποιήσετε τον παραπάνω σύνδεσμο για να έχετε αυτόματα τη σωστή έκδοση HTML &mdash; δείτε τη σελίδα <a href="$(HOME)/intro/cn">διαπραγμάτευση περιεχομένου</a>. Διαφορετικά, επιλέξτε τη σωστή αρχιτεκτονική, γλώσσα και μορφοποίηση που θέλετε από τον παρακάτω πίνακα.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Αρχιτεκτονική</strong></th>
  <th align="left"><strong>Μορφοποίηση</strong></th>
  <th align="left"><strong>Γλώσσες</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
