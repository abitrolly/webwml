#use wml::debian::template title="Τεκμηρίωση"
#use wml::debian::translation-check translation="9aa7cfecde4026c09b63f9d42fa74cbe0f7e50bb" maintainer="galaxico"

<p>Ένα σημαντικό μέρος οποιουδήποτε λειτουργικού συστήματος είναι η τεκμηρίωση, 
τα τεχνικά εγχειρίδια που περιγράφουν τη λειτουργία και τη χρήση των 
προγραμμάτων. Ως μέρος της προσπάθειάς του να δημιουργήσει ένα υψηλής ποιότητας 
ελεύθερο λειτουργικό σύστημα, το Σχέδιο Debian καταβάλει κάθε 
προσπάθεια να προσφέρει σε όλους τους χρήστες του πραγματική τεκμηρίωση σε 
εύκολα προσβάσιμη μορφή.</p>

<h2>Ξεκινήστε στα γρήγορα</h2>

<p>Αν είστε <em>καινούριος/α</em> στο Debian συνιστούμε να ξεκινήσετε 
διαβάζοντας πρώτα:</p>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">τον Οδηγό 
Εγκατάστασης</a></li>
  <li><a href="manuals/debian-faq/">Συχνές ερωτήσεις (FAQ) του Debian 
GNU/Linux</a></li>
</ul>

<p>Είναι καλό να έχετε εύκαιρα αυτά τα δύο κείμενα όταν κάνετε την πρώτη σας 
εγκατάσταση Debian, πιθανόν θα σας απαντήσουν πολλές ερωτήσεις και θα σας 
βοηθήσουν να δουλέψετε με το καινούριο Debian σύστημά σας. Αργότερα ίσως να 
θέλετε να δείτε και τα:</p>

<ul>
  <li><a href="manuals/debian-handbook/">The Debian Administrator's 
Handbook</a>, το περιεκτικό εγχειρίδιο χρήστη</li>
  <li><a href="manuals/debian-reference/">Debian Reference</a>,
      ένας λιτός οδηγός με εστίαση στη γραμμή εντολών</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">Σημειώσεις Έκδοσης</a>,
      για όσους/όσες αναβαθμίζουν το σύστημά τους</li>
  <li><a href="https://wiki.debian.org/">Debian Wiki</a>, μια καλή πηγή 
πληροφοριών για νεοεισερχόμενες/ους</li>
</ul>


<p>Τέλος, βεβαιωθείτε ότι έχετε τυπώσει και έχετε πρόχειρη την <a
href="https://www.debian.org/doc/manuals/refcard/refcard">Κάρτα Αναφοράς 
του Debian GNU/Linux</a>, μια λίστα με τις πιο σημαντικές εντολές για 
συστήματα Debian.</p>

<p>Υπάρχουν αρκετές ακόμα πηγές τεκμηρίωσης στη λίστα που ακολουθεί.</p>

<h2>Τύποι τεκμηρίωσης</h2>

<p>Το μεγαλύτερο μέρος της τεκμηρίωσης που συμπεριλαμβάνεται στο Debian έχει 
γραφτεί γενικά για το GNU/Linux. Υπάρχουν επίσης κάποιες πηγές τεκμηρίωσης που 
έχει γραφτεί ειδικά για το Debian. Αυτά τα κείμενα χωρίζονται στις παρακάτω 
βασικές κατηγορίες:</p>

<ul>
  <li><a href="#manuals">εγχειρίδια</a></li>
  <li><a href="#howtos">HOWTO</a></li>
  <li><a href="#faqs">Συχνές ερωτήσεις (FAQ)</a></li>
  <li><a href="#other">άλλα μικρότερα κείμενα</a></li>
</ul>

<h3 id="manuals">Εγχειρίδια</h3>

<p>Τα εγχειρίδια μοιάζουν με βιβλία, γιατί περιγράφουν σε έκταση κάποιο 
μείζονα θέματα.</p>

<h3>Εγχειρίδια ειδικά για το Debian</h3>

<div class="line">
  <div class="item col50">
    
    <h4><a href="user-manuals">Εγχειρίδια χρηστών</a></h4>
    <ul>
      <li><a href="user-manuals#faq">Συχνές ερωτήσεις του Debian 
GNU/Linux</a></li>
      <li><a href="user-manuals#install">Οδηγός εγκατάστασης του Debian</a></li>
      <li><a href="user-manuals#relnotes">Σημειώσεις Έκδοσης του Debian</a></li>
      <li><a href="user-manuals#refcard">Κάρτα Αναφοράς του Debian</a></li>
      <li><a href="user-manuals#debian-handbook">Το εγχειρίδιο του 
Διαχειριστή του Debian</a></li>
      <li><a href="user-manuals#quick-reference">Αναφορά του Debian</a></li>
      <li><a href="user-manuals#securing">Εγχειρίδιο Ασφάλειας 
του Debian</a></li>
      <li><a href="user-manuals#aptitude">Εγχειρίδιο χρήστη του aptitude 
</a></li>
   <li><a href="user-manuals#apt-offline">Χρησιμοποιώντας το APT εκτός δικτύου</a></li>
      <li><a href="user-manuals#java-faq">Συχνές ερωτήσεις του Debian GNU/Linux 
και της Java</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Οδηγός Συντηρητή του Debian Hamradio</a></li>
    </ul>

  </div>
  
  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Εγχειρίδια Προγραμματιστριών/στών</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Εγχειρίδιο πολιτικής του Debian</a></li>
      <li><a href="devel-manuals#devref">Αναφορά Προγραμματιστριών/στών 
του Debian</a></li>
      <li><a href="devel-manuals#debmake-doc"> Οδηγός για τους Συντηρητές 
του Debian</a></li>
      <li><a href="devel-manuals#maint-guide">Οδηγός για Νέους Συντηρητές 
του Debian</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Εισαγωγή στη δημιουργία 
πακέτων για to Debian</a></li>
      <li><a href="devel-manuals#menu">Το Μενού Συστήματος του Debian</a></li>
      <li><a href="devel-manuals#d-i-internals">Ενδότερα του Εγκαταστάτη 
του Debian</a></li>
      <li><a href="devel-manuals#dbconfig-common">Οδηγός για 
Συντηρητές/τριες πακέτων που χρησιμοποιούν βάσεις δεδομένων</a></li>
      <li><a href="devel-manuals#dbapp-policy">Πολιτική για πακέτα που 
χρησιμοποιούν βάσεις δεδομένων</a></li>
    </ul>

    <h4><a href="misc-manuals">Διάφορα εγχειρίδια</a></h4>
    <ul>
      <li><a href="misc-manuals#history">Ιστορία του Σχεδίου Debian</a></li>
    </ul>

  </div>


</div>

<p class="clr">Η πλήρης λίστα των εγχειριδίων του Debian και άλλης τεκμηρίωσης 
μπορεί να βρεθεί στις ιστοσελίδες του <a href="ddp">Σχεδίου Τεκμηρίωσης 
του Debian</a></p>

<p>Υπάρχουν επίσης αρκετά εγχειρίδια κυρίως για χρήστες γραμμένα για το Debian 
GNU/Linux, και διαθέσιμα ως <a href="books">έντυπα βιβλία</a>.</p>

<h3 id="howtos">HOWTO</h3>

<p>Τα <a 
href="https://www.tldp.org/HOWTO/HOWTO-INDEX/categories.html">Κείμενα HOWTO</a>, 
όπως λέει και το όνομά τους, περιγράφουν <em>πώς να</em> κάνετε κάτι, και, 
συνήθως, καλύπτουν ένα πιο συγκεκριμένο θέμα.</p>


<h3 id="faqs">Συχνές ερωτήσεις (FAQ)</h3>

<p>Η λέξη FAQ στα Αγγλικά είναι ακρωνύμιο των λέξεων <em>frequently asked 
questions (συχνές ερωτήσεις)</em>. Ένα κείμενο συχνών ερωτήσεων (FAQ) είναι 
ένα κείμενο που απαντά σ' αυτές τις ερωτήσεις.</p>

<p>Ερωτήσεις ειδικά σχετιζόμενες με το Debian απαντώνται στο εγχειρίδιο
<a href="manuals/debian-faq/">Debian FAQ</a>.
Υπάρχει επίσης μια ξεχωριστή ενότητα για <a href="../CD/faq/">Συχνές 
ερωτήσεις (FAQ) σχετικά με τις εικόνες των CD/DVD του Debian</a>.</p>


<h3 id="other">Άλλα, συντομότερα κείμενα</h3>

<p>Τα ακόλουθα κείμενα περιέχουν πιο γρήγορες, συντομότερες οδηγίες:</p>

<dl>

  <dt><strong><a 
href="http://www.tldp.org/docs.html#man">Σελίδες εγχειριδίων 
(manual pages)</a></strong></dt>
    <dd>Παραδοσιακά, όλα τα προγράμματα του Unix τεκμηριώνονται με <em>manual
        pages</em>, εγχειρίδια αναφοράς που είναι διαθέσιμα μέσω της εντολής 
<tt>man</tt>. Συνήθως δεν απευθύνονται σε νέους χρήστες. Μπορείτε να κάνετε 
αναζήτηση και να διαβάσετε τις σελίδες αυτές που είναι διαθέσιμες για το Debian 
στην ιστοσελίδα
<a 
href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org 
/</a>.
    </dd>

  <dt><strong><a 
href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">
Αρχεία info</a></strong></dt>
    <dd>Αρκετό λογισμικό του GNU τεκμηριώνεται μέσω αρχείων <em>info</em> 
αντί σελίδων manual pages. Αυτά τα αρχεία περιλαμβάνουν λεπτομερείς πληροφορίες 
για το ίδιο το πρόγραμμα, επιλογές και παραδείγματα χρήσης και είναι διαθέσιμα 
μέσω της εντολής <tt>info</tt>.
    </dd>

  <dt><strong>διάφορα αρχεία README</strong></dt>
    <dd>Τα αρχεία <em>read me</em> είναι επίσης αρκετά συνηθισμένα &mdash; 
είναι απλά αρχεία κειμένου που περιγράφουν ένα συγκεκριμένο στοιχείο, 
συνήθως ένα πακέτο. Μπορείτε να βρείτε αρκετά από αυτά στους υποκαταλόγους
        <tt>/usr/share/doc/</tt> στο Debian σύστημά σας. Κάθε πακέτο λογισμικού 
έχει έναν υποκατάλογο κάτω από αυτό που με τα δικά του αρχεία read me, και ίσως 
περιέχει επίσης παραδείγματα ρυθμίσεων. Σημειώστε ότι, για μεγαλύτερα 
προγράμματα, η τεκμηρίωση παρέχεται τυπικά σε ένα ξεχωριστό πακέτο (με το 
ίδιο όνομα με το αρχικό πακέτο αλλά την κατάληξη <em>-doc</em>).
    </dd>

  <dt><strong>σύντομες κάρτες αναφοράς</strong></dt>
    <dd>
        <p>Σύντομες κάρτες αναφοράς είναι πολύ μικρές περιλήψεις για ένα 
συγκεκριμένο (υπο)σύστημα. Συνήθως, μια τέτοια κάρτα αναφοράς παρέχει τις πιο 
συχνά χρησιμοποιούμενες εντολές σε ένα μοναδικό κομμάτι χαρτί. Μερικές 
αξιοσημείωτες κάρτες αναφοράς και συλλογές περιλαμβάνουν:</p>
        <dl>
          <dt><a 
href="https://www.debian.org/doc/manuals/refcard/refcard">Κάρτα Αναφοράς 
του Debian
              GNU/Linux</a></dt>
	    <dd>Αυτή η κάρτα, που μπορεί να εκτυπωθεί σε μια σελίδα, παρέχει μια 
λίστα με τις πιο σημαντικές εντολές και είναι μια καλή αναφορά για νέους 
χρήστες στο Debian που θέλουν να εκξοικειωθούν με αυτές. Απαιτείται μια 
τουλάχιστον βασική γνώση υπολογιστών, αρχείων, καταλόγων και της γραμμής 
εντολών. Νέοι χρήστες ίσως θελήσουν να διαβάσουν πριν το εγχειρίδιο
		<a href="user-manuals#quick-reference">Αναφορά Debian</a>.</dd>
	</dl>
    </dd>

</dl>


<hrline />

<p>Αν έχετε ελέγξει τις παραπάνω πηγές και εξακλολυθείτε να μην έχετε βρει 
απαντήσεις ή λύσεις στα προβλήματά σας σχετικά με το Debian, ρίξτε μια ματιά 
στη σελίδα μας για την <a href="../support">Υποστήριξη</a>.</p>
