#use wml::debian::template title="Porte SPARC" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/sparc/menu.inc"
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<h1>Porte Debian SPARC</h1>

<ul>
 <li><a href="#intro">Visão geral</a></li>
 <li><a href="#status">Estado atual</a></li>
 <li><a href="#sparc64bit">Sobre o suporte ao SPARC 64-bit</a>
 <ul>
   <li><a href="#kernelsun4u">Compilando kernels para sun4u</a></li>
 </ul></li>
 <li><a href="#errata">Errata</a></li>
 <li><a href="#who">Quem somos nós? Como eu posso ajudar?</a></li>
 <li><a href="#links">Onde eu posso encontrar mais informações?</a></li>
</ul>

<h2 id="intro">Visão geral</h2>
  <p>
Estas páginas objetivam auxiliar usuários(as) e desenvolvedores(as) Debian a
rodar o Debian GNU/Linux na arquitetura SPARC. Nestas páginas, você encontrará
informações sobre o estado atual, problemas atualmente conhecidos,
informações para e sobre a equipe de portabilidade do Debian, e
apontamentos para mais informações.
  </p>

<h2 id="status">Estado atual</h2>
  <p>
O Debian SPARC está oficialmente lançado e é conhecido por ser estável.
Suportado por máquinas sun4u e sun4v (com um espaço de usuário(a) de 32-bit).
Veja o <a href="../../releases/stable/sparc/">manual de instalação</a>
para informações sobre sistemas suportados, hardware e como instalar
o Debian.
  </p>


<h2 id="sparc64bit">Sobre o suporte ao SPARC 64-bit</h2>
  <p>
O porte do Debian SPARC,
como mencionado acima, tem suporte às arquiteturas sun4u (<q>Ultra</q>)
e sun4v (CPU Niagara).
Ele usa um kernel 64-bit (compilado com gcc 3.3 ou mais novo), mas a maioria das
aplicações roda em 32-bit. Isto também é chamado de um <q>espaço de usuário(a)
de 32-bit</q>.
  </p>
  <p>
O esforço de portar o Debian SPARC 64 (também conhecido como <q>UltraLinux</q>)
atualmente não é concebido como um trabalho de portabilidade completo como os
outros portes. Ao contrário, planeja-se que ele seja um <em>add-on</em>
para o porte SPARC.
  </p>
  <p>
De fato, não há motivo para termos todas as aplicações executando
no modo 64-bit. O modo 64-bit completo envolve um custo significante
(memória e tamanho em disco) e geralmente nenhum benefício. Algumas
aplicações realmente podem se beneficiar de estarem no modo 64-bit,
e este é o propósito para este esforço de portabilidade.
  </p>

<h3 id="kernelsun4u">Compilando kernels para sun4u</h3>
  <p>
Para compilar um kernel Linux para Sun4u, você precisará usar a árvore de
código-fonte do Linux 2.2 ou mais nova.
  </p>
  <p>
Nós fortemente sugerimos que você também use o pacote <tt>kernel-package</tt>
para ajudar com a instalação e o gerenciamento de kernels. Você pode compilar
um kernel configurado com um comando (como root):
  </p>
<pre>
  make-kpkg --subarch=sun4u --arch_in_name --revision=custom.1 kernel_image
</pre>


<h2 id="errata">Errata</h2>
  <p>
Alguns dos problemas comuns com correções ou soluções alternativas podem
ser encontrados em nossa
<a href="problems">página de erratas</a>.
  </p>


<h2 id="who">Quem somos nós? Como eu posso ajudar?</h2>
  <p>
O porte do Debian SPARC é um esforço distribuído, assim como é o Debian.
Inúmeras pessoas têm ajudado com o porte e com o esforço de
documentação, embora uma lista curta de <a href="credits">créditos</a> esteja
disponível.
  </p>
  <p>
Se você gostaria de ajudar, por favor,
junte-se à lista de discussão  &lt;debian-sparc@lists.debian.org&gt; como
<a href="#links">descrito abaixo</a>, e nos chame.
  </p>
  <p>
Desenvolvedores(as) registrados(as) que, de modo ativo, desejem portar e
fazer upload de pacotes portados, devem ler as diretrizes da equipe de porte
na <a href="$(DOC)/developers-reference/">referência de
desenvolvedores(as)</a> e ver a <a href="porting">página de porte do SPARC</a>.
  </p>


<h2 id="links">Onde eu posso encontrar mais informações?</h2>
  <p>
O melhor lugar para fazer perguntas específicas ao Debian sobre o porte SPARC,
é na lista de discussão, <a href="https://lists.debian.org/debian-sparc/">\
&lt;debian-sparc@lists.debian.org&gt;</a>.
Os <a
href="https://lists.debian.org/debian-sparc/">arquivos</a> da lista de
discussão são navegáveis pela web.
  </p>
  <p>
Para se inscrever na lista, envie um e-mail para
<a href="mailto:debian-sparc-request@lists.debian.org">\
debian-sparc-request@lists.debian.org</a>, com a palavra "subscribe"
na linha de assunto, e sem texto no corpo da mensagem. Alternativamente,
inscreva-se pela web na página
<a href="https://lists.debian.org/debian-sparc/">de inscrição da
lista de discussão</a>.
  </p>
  <p>
Questões sobre o kernel devem ser endereçadas para a lista
&lt;sparclinux@vger.rutgers.edu&gt;. Inscreva-se enviando uma mensagem
com o texto <q>subscribe sparclinux</q> no corpo do e-mail para o
endereço <a href="mailto:majordomo@vger.rutgers.edu">majordomo@vger.rutgers.edu</a>.
Também existe uma lista da Red Hat, claro.
  </p>
  <p>
Esta é uma lista de links muito pequena do Linux SPARC (também conhecido como
<q>S/Linux</q>):
  </p>
 <ul>
      <li>
<a href="http://www.ultralinux.org/">UltraLinux</a> -- a
fonte definitiva para o porte do kernel. Não engane-se pelo nome;
ela cobre majoritariamente o SPARC, mais que o UltraSPARC.</li>
 </ul>

