#use wml::debian::template title="Canto dos(as) desenvolvedores(as) Debian" BARETITLE="true"
#use wml::debian::translation-check translation="2a91a8cd41f14166dc4ef23023d68bc2825268ff"

<p>As informações nesta página, apesar de públicas, serão principalmente de
interesse dos(as) desenvolvedores(as) Debian.</p>

<ul class="toc">
<li><a href="#basic">Básico</a></li>
<li><a href="#packaging">Empacotamento</a></li>
<li><a href="#workinprogress">Trabalhos em andamento</a></li>
<li><a href="#projects">Projetos</a></li>
<li><a href="#miscellaneous">Diversos</a></li>
</ul>

<div id="main">
  <div class="cardleft" id="basic">
  <h2>Básico</h2>
      <div>
      <dl>
        <dt><a href="$(HOME)/intro/organization">Organização do Debian</a></dt>

        <dd>
        O Debian tem muitos pontos de acesso, e muitas pessoas envolvidas. Esta
        página explica quem contatar sobre um aspecto específico do Debian, e
        te informa quem deverá responder.
        </dd>

        <dt>As pessoas</dt>
        <dd>
        O Debian é feito de forma colaborativa por muitas pessoas espalhadas
        pelo mundo. O <em>trabalho de empacotamento</em> é geralmente
        realizado tanto por <a href="https://wiki.debian.org/DebianDeveloper">
        desenvolvedores(as) Debian (DD)</a> (que são membros(as) plenos(as) do projeto
        Debian) quanto por <a href="https://wiki.debian.org/DebianMaintainer">
        mantenedores(as) Debian (DM)</a>. Aqui você pode encontrar tanto a <a
        href="https://nm.debian.org/public/people/dd_all">lista de
        desenvolvedores(as) Debian</a> quanto a <a
        href="https://nm.debian.org/public/people/dm_all">lista de
        mantenedores(as) Debian</a>, juntamente com os pacotes que eles(as)
        mantêm.

        <p>
        Você também pode ver o <a href="developers.loc">mapa-múndi dos(as)
        desenvolvedores(as) Debian</a> e a <a href="https://gallery.debconf.org/">
        galeria de imagens</a> de vários eventos Debian.
        </p>
        </dd>

        <dt><a href="join/">Entrando para o Debian</a></dt>

        <dd>
        O Projeto Debian é formado por voluntários(as), e nós estamos geralmente
        procurando por novos(as) desenvolvedores(as) que tenham algum conhecimento
        técnico, interesse em software livre e algum tempo livre. Você também
        pode ajudar o Debian, veja a página acima.
        </dd>

        <dt><a href="https://db.debian.org/">Banco de dados de desenvolvedores(as)</a></dt>
        <dd>
        O banco de dados contém dados básicos acessíveis a todos(as), e dados
        mais privados acessíveis apenas para desenvolvedores(as). Use a
        <a href="https://db.debian.org/">versão SSL</a> para acessá-lo se você
        pretende fazer login.

        <p>Usando o banco de dados, você pode ver uma lista de
        <a href="https://db.debian.org/machines.cgi">máquinas do projeto</a>,
        <a href="extract_key">obter a chave GPG de qualquer desenvolvedor(a)</a>,
        <a href="https://db.debian.org/password.html">mudar sua senha</a>
        ou <a href="https://db.debian.org/forward.html">aprender como configurar
        o redirecionamento do e-mail</a> para sua conta Debian.</p>

        <p>Se você pretende usar alguma das máquinas do Debian certifique-se
        de ler as <a href="dmup">políticas de uso das máquinas Debian</a>.</p>
        </dd>

        <dt><a href="constitution">A Constituição</a></dt>
        <dd>
	É o documento de maior importância para a organização, descrevendo
        a estrutura organizacional para tomadas de decisões formais no
        Projeto.
        </dd>

        <dt><a href="$(HOME)/vote/">Informações sobre votações</a></dt>
        <dd>
        Tudo que você sempre quis saber sobre como elegemos nossos(as) líderes,
        escolhemos nossos logotipos e, em geral, como votamos.
        </dd>
     </dl>

# this stuff is really not devel-only
     <dl>
        <dt><a href="$(HOME)/releases/">Versões (<q>Releases</q>)</a></dt>

        <dd>
        Essa é a lista de versões antigas e atuais, algumas das quais
        têm informações detalhadas em páginas separadas.

        <p>Você também pode ir diretamente para as páginas web da
        <a href="$(HOME)/releases/stable/">versão estável (<q>stable</q>)</a> e
        da <a href="$(HOME)/releases/testing/">versão teste
        (<q>testing</q>)</a>.</p>
        </dd>

        <dt><a href="$(HOME)/ports/">Arquiteturas diferentes</a></dt>

        <dd>
        O Debian roda em muitos tipos de computadores (compatível com
        Intel foi apenas o <em>primeiro</em> tipo), e os(as) mantenedores(as)
        de nossos &lsquo;portes&rsquo; têm algumas páginas úteis. Dê uma
        olhada, talvez você irá querer comprar algum outro pedaço de hardware
        com nome estranho para você.
	</dd>
      </dl>
      </div>

  </div>

  <div class="cardright" id="packaging">
     <h2>Empacotamento</h2>
     <div>

      <dl>
        <dt><a href="$(DOC)/debian-policy/">Manual de políticas Debian</a></dt>
        <dd>
        Esse manual descreve os requisitos das políticas para a distribuição
        Debian. Isso inclui a estrutura e o conteúdo do repositório Debian,
        várias questões de design do sistema operacional, assim
        como requisitos técnicos que cada pacote deve satisfazer para ser
        incluído na distribuição.

        <p>Resumindo, você <strong>precisa</strong> ler este manual.</p>
        </dd>
      </dl>

      <p>Há vários documentos relacionados à política que devem ser de
      seu interesse, como:</p>
      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />O FHS é uma lista dos diretórios (ou arquivos) onde as coisas
            devem ser colocadas, e a compatibilidade com ele é requerida pela
            política 3.x.</li>
        <li>Lista de <a href="$(DOC)/packaging-manuals/build-essential">pacotes build-essential</a>
        <br />Os pacotes build-essential são pacotes que se espera que você
            tenha antes de tentar construir qualquer pacote, ou um conjunto de
            pacotes que você não tem de incluir na linha
            <code>Build-Depends</code> do seu pacote.</li>
	<li><a href="$(DOC)/packaging-manuals/menu-policy/">Sistema de menus</a>
        <br />Programas que têm uma interface e que não precisam ter
            argumentos especiais na linha de comando para um funcionamento
            normal devem ter uma entrada de menu registrada.
            Verifique também a <a href="$(DOC)/packaging-manuals/menu.html/">\
            documentação do sistema de menus</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Políticas do Emacs</a>
        <br />Espera-se que os pacotes relacionados ao Emacs respeitem seu
            próprio documento de subpolíticas.</li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Políticas Java</a>
        <br />O equivalente ao proposto acima, para pacotes relacionados a
            Java.</li>
	<li><a href="$(DOC)/packaging-manuals/perl-policy/">Política Perl</a>
        <br />Uma subpolítica que cobre tudo relacionado a empacotamento Perl.</li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Política Python</a>
        <br />Uma subpolítica proposta que cobre tudo sobre o empacotamento Python.</li>
#       <li><a href="https://pkg-mono.alioth.debian.org/cli-policy/">Políticas Debian CLI</a>
#       <br />Políticas básicas com relação ao empacotamento Mono, outras CLRs e
#        aplicações baseadas em CLI e bibliotecas</li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Especificação da Debconf</a>
        <br />A especificação para o subsistema de gerenciamento de configuração
            "debconf".</li>
#        <li><a href="https://dict-common.alioth.debian.org/">Política para dicionários e ferramentas de correção ortográfica</a>
#        <br />Subpolítica para dicionários <kbd>ispell</kbd> / <kbd>myspell</kbd> e listas de palavras.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft/html/">Manual de políticas para aplicações web</a> (rascunho)
#        <br />Subpolíticas para aplicações baseadas na web.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft-php/html/">Políticas PHP</a> (rascunho)
#        <br />Padrões de empacotamento do PHP.</li>
         <li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Políticas de aplicações de bancos de dados</a> (rascunho)
         <br />Um conjunto de diretrizes e melhores práticas para pacotes de aplicações de banco de dados.</li>
         <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Política Tcl/Tk</a> (rascunho)
         <br />Subpolítica que cobre tudo com relação ao empacotamento Tcl/Tk.</li>
	 <li><a
	 href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Política
	 do Debian para Ada</a>
	 <br />Subpolítica que cobre tudo com relação ao empacotamento Ada.</li>
      </ul>

      <p>Dê uma olhada também nas <a
      href="https://bugs.debian.org/debian-policy">propostas de mudanças
      para as políticas</a>.</p>

      <p>Note que o antigo manual de empacotamento foi integrado principalmente
      às versões recentes do manual de políticas.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">
        Referência dos(as) desenvolvedores(as)</a></dt>

        <dd>
        O propósito desse documento é fornecer uma visão geral dos procedimentos
        recomendados e os recursos disponíveis para os(as) desenvolvedores(as)
        Debian. Outro de leitura obrigatória.
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">Guia dos(as) novos(as) mantenedores(as)</a></dt>

        <dd>
        Esse documento descreve a construção de um pacote Debian em uma
        linguagem comum e bem coberta por exemplos que funcionam. Se você
        é um(a) pretendente a desenvolvedor(a) (empacotador(a)), você
        definitivamente vai querer ler isso.
        </dd>
      </dl>
      </div>

  </div>

  <div class="card" id="workinprogress">
      <h2>Trabalhos&nbsp;em&nbsp;andamento</h2>
      <div>

	<dl>
        <dt><a href="testing">A versão testing</a></dt>
        <dd>
        A versão &lsquo;testing&rsquo; é o local onde seus pacotes
        devem estar para serem inseridos na próxima vez que o Debian
        realizar um lançamento.
        </dd>

        <dt><a href="https://bugs.debian.org/release-critical/">Bugs críticos ao lançamento</a></dt>

        <dd>
        Essa é uma lista de bugs que podem fazer com que um pacote seja
        removido da versão <q>testing</q>, ou em alguns casos
        causar um atraso no lançamento da distribuição. Relatórios de
        bugs com uma severidade maior ou igual a &lsquo;serious&rsquo;
        qualificam-se para a lista -- certifique-se de corrigir tais bugs
        nos seus pacotes assim que você puder.
        </dd>

        <dt><a href="$(HOME)/Bugs/">O sistema de acompanhamento de bugs</a></dt>
        <dd>
        O Debian <q>Bug Tracking System (BTS)</q> é o sistema de acompanhamento de
        bugs do Debian, para relatar, discutir e corrigir bugs. Relatórios de
        problemas em praticamente qualquer parte do Debian são bem-vindos
        aqui. O BTS é útil tanto para usuários(as) quanto para
        desenvolvedores(as).
        </dd>

        <dt>Visão geral dos pacotes, do ponto de vista de um(a) desenvolvedor(a)</dt>
        <dd>
        As páginas de <a href="https://qa.debian.org/developer.php">
        informações de pacotes</a> e <a href="https://tracker.debian.org/">\
        acompanhamento de pacotes</a> fornecem conjuntos de informações valiosas
        para os(as) mantenedores(as).
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources#pkg-tracker">
        O sistema de acompanhamento de pacotes</a></dt>
        <dd>
        Para desenvolvedores(as) que desejam ficar atualizados(as) sobre outros
        pacotes, o sistema de acompanhamento de pacotes (<q>package tracker</q>)
        permite que eles(as) se inscrevam (por e-mail) em um serviço que enviará
        cópias dos e-mails do BTS e notificações de uploads e questões
        instalações dos pacotes selecionados.
        </dd>

        <dt><a href="wnpp/">Pacotes que precisam de ajuda</a></dt>
        <dd>
        Work-Needing and Prospective Packages, WNPP para encurtar (em
        português, pacotes que precisam de trabalho e futuros pacotes), é uma lista
        de pacotes Debian que precisam de um(a) novo(a) mantenedor(a) ou que ainda
        não estão incluídos no Debian. Verifique essa página se você deseja
        criar, adotar ou abandonar pacotes.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">\
          Sistema <q>Incoming</q></a></dt>
        <dd>
        Novos pacotes são enviados para o sistema <q>Incoming</q> nos
        servidores de repositórios internos. Pacotes aceitos são quase que
        imediatamente <a href="https://incoming.debian.org/">disponibilizados via HTTP</a>,
        e propagados para os <a href="$(HOME)/mirror/">espelhos</a> quatro
        vezes por dia.
        <br />
        <strong>Nota</strong>: por causa da natureza do <q>Incoming</q>, nós
        não recomendamos espelhá-lo.
        </dd>

        <dt><a href="https://lintian.debian.org/">Relatórios Lintian</a></dt>

        <dd>
        O <a href="https://packages.debian.org/unstable/devel/lintian">
        Lintian</a> é um programa que checa se um pacote está de acordo com
        as políticas Debian. Você deve usá-lo antes de cada upload;
        existem relatórios na página acima de cada pacote da distribuição.
        </dd>

        <dt><a href="https://wiki.debian.org/HelpDebian">Ajuda Debian</a></dt>
        <dd>
        A wiki do Debian reúne conselhos para desenvolvedores(as) e outros(as)
        contribuidores(as).
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources#s4.6.4">\
            Distribuição <q>Experimental</q></a></dt>
        <dd>
        A distribuição <em>experimental</em> é usada como uma área temporária
        para softwares altamente experimentais. Use os
        <a href="https://packages.debian.org/experimental/">pacotes da
        <em>experimental</em></a> somente se você já sabe como usar a
        <em>instável (unstable)</em>.
        </dd>
      </dl>
      </div>

  </div>
  <div class="card" id="projects">
     <h2>Projetos</h2>
     <div>

      <p>O Debian é um grupo grande e assim ele consiste de vários
      grupos e projetos internos. Aqui estão aqueles que têm páginas
      web, organizados cronologicamente:</p>
      <ul>
          <li><a href="website/">Páginas web do Debian</a></li>
          <li><a href="https://ftp-master.debian.org/">Repositório Debian</a></li>
          <li><a href="$(DOC)/ddp">Projeto de documentação Debian (DDP)</a></li>
          <li>O grupo de <a href="https://qa.debian.org/">Controle
              de qualidade</a></li>
          <li><a href="$(HOME)/CD/">Imagens de CD do Debian</a></li>
          <li>A página de coordenação <a href="https://wiki.debian.org/Keysigning">
             de assinatura de chaves</a>.</li>
          <li><a href="https://wiki.debian.org/DebianIPv6">Projeto
             Debian IPv6</a></li>
          <li><a href="buildd/">Rede auto-builder</a> e seus
          <a href="https://buildd.debian.org/">logs de construção</a></li>
          <li><a href="$(HOME)/international/l10n/ddtp">Projeto de tradução das
             descrições Debian (DDTP)</a></li>
      <li><a href="debian-installer/">O instalador Debian</a></li>
      <li><a href="debian-live/">Debian Live</a></li>
      <li><a href="$(HOME)/women/">Debian mulheres</a></li>
      <li><a href="$(HOME)/blends/">Debian Pure Blends</a></li>

	</ul>
	</div>

  </div>

  <div class="card" id="miscellaneous">
      <h2>Diversos</h2>
      <div>

      <p>Outros links:</p>
      <ul>
        <li><a href="https://debconf-video-team.pages.debian.net/videoplayer/">Gravações</a> das nossas palestras.</li>
        <li><a href="passwordlessssh">Configurando o ssh para não solicitar
        uma senha</a>.</li>
        <li>Como <a href="$(HOME)/MailingLists/HOWTO_start_list">solicitar uma
            nova lista de discussão</a>.</li>
        <li>Informações sobre <a href="$(HOME)/mirror/">o espelhamento do Debian</a>.</li>
        <li>O <a href="https://qa.debian.org/data/bts/graphs/all.png">gráfico
            de todos os bugs</a>.</li>
	<li><a href="https://ftp-master.debian.org/new.html">Novos pacotes que
            esperam para serem incluídos no Debian</a> (NOVA fila).</li>
        <li><a href="https://packages.debian.org/unstable/main/newpkg">Novos
            pacotes Debian nos últimos 7 dias</a>.</li>
        <li><a href="https://ftp-master.debian.org/removals.txt">Pacotes
            removidos do Debian</a>.</li>
        </ul>

      </div>

  </div>
</div>
