#use wml::debian::template title="Materiais e merchandise para eventos do Debian" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="a94c94e8b45e670b63006e4e81cebb3a3690b5a3"

<p>
Esta página contém uma lista de todos os diferentes materiais de merchandising
que as pessoas que organizam os estandes Debian podem usar (de modo que não seja
preciso refazê-la do zero):
</p>

<toc-display />


<toc-add-entry name="posters">Pôsteres</toc-add-entry>

<p>
Pôsteres realmente trazem elegância para o estande, então você deve
reservar um tempo e imprimir alguns. Além disso, se tomar os devidos cuidados,
eles podem ser usados de tempos em tempos em diferentes eventos. Se não quiser
mantê-los, sempre pode-se doá-los em uma reunião BOF (Birds of a Feather -
desconferência).
</p>

<p>
<a href="mailto:arti@kumria.com">Arti Kumria</a> elaborou os seguintes
pôsteres para a <a href="http://www.linuxexpo.com.au/">LinuxExpo Austrália</a>
que aconteceu em março de 2000 em Sidnei:
</p>

<ul>
  <li><a href="materials/posters/poster1.pdf">poster1.pdf</a> [182 kB]
  <li><a href="materials/posters/poster1.ps.gz">poster1.ps.gz</a> [16.3 MB]
  <li><a href="materials/posters/poster2.pdf">poster2.pdf</a> [175 kB]
  <li><a href="materials/posters/poster2.ps.gz">poster2.ps.gz</a> [16.3 MB]
</ul>

<p>
A pré-visualização dos arquivos PDF dá uma boa ideia de como o pôster se parece,
exceto pela espiral Debian que fica rosa em vez de vermelha. Ela fica legal
quando impressa (mas certifique-se e faça uma prova de impressão).
</p>

<p>
Os pôsteres acima estão licenciados sob domínio público. Se quiser obter
os dados originais usados na criação, favor contatar
<a href="mailto:akumria@debian.org">Anand Kumria</a>.
</p>

<p>
Na Alemanha, <a href="mailto:joey@debian.org">Joey</a> tem uns
<a href="https://cvs.infodrom.org/goodies/posters/?cvsroot=debian">pôsteres</a>
que ele pode enviar pelos correios, mas somente para empréstimo e somente para
um estande Debian em uma exposição.
</p>

<p>
Nos Estados Unidos, Don Armstrong tem um conjunto de banners.
</p>

<p>
Thimo Neubauer também forneceu um
<a href="materials/posters/worldmap/">mapa-múndi</a> com coordenadas dos(as)
desenvolvedores(as) Debian.
</p>

<p>
Alexis Younes do 73lab.com também elaborou uns pôsteres muito legais. Eles
estavam disponíveis para download no site dele [link para download indisponível
em 29/08/2020] Alguns pôsteres impressos estão disponíveis na Alemanha; por
favor, pergunte na
<a href="mailto:debian-events-eu@lists.debian.org">lista debian-events-eu</a>.
A propósito: eles vendem bem, então talvez você queira imprimir alguns a
mais :)
</p>

<toc-add-entry name="flyers">Folhetos e panfletos</toc-add-entry>

<p>
As pessoas gostam de levar coisas dos estandes, e mesmo que você não tenha
condições de dar brindes caros (veja abaixo), sempre pode-se fornecer folhetos.
Eles têm a capacidade de responder muitas questões sobre o Debian, podem atrair
pessoas para contribuir, e por ser papel ainda podem servir de bloco de notas!
De todo jeito, folhetos são muito úteis.
</p>

<p>
Fontes para folhetos multilíngues, com também instruções para impressão, estão
disponíveis na
<a href="https://debian.pages.debian.net/debian-flyers/">página web de folhetos Debian</a>.
</p>

<p>
Para panfletos, existem <a href="materials/pamphlets/">arquivos AbiWord</a>
disponíveis tanto em inglês quanto em espanhol que podem ser facilmente
traduzidos para qualquer idioma.
</p>

<p>
Na Europa, Michael Meskes mantém uma versão impressa e bilíngue (inglês e
alemão) de folhetos elaborados pelo pessoal do Debian durante as preparações
para a <a href="2002/0606-linuxtag">LinuxTag 2002</a>. Mesmo que seu evento
ocorra em um país em que nenhum desses idiomas sejam línguas nativas, eles
provavelmente vão ajudar as pessoas a entender o projeto Debian, e você sempre
pode traduzi-los para seu idioma. Esses folhetos impressos podem ser requisitados
para o <a href="mailto:meskes@debian.org">Michael Meskes</a>.
</p>

<toc-add-entry name="demos">Sistemas de demonstração</toc-add-entry>

<p>
Claro que é sempre uma boa ideia preparar um sistema que rode Debian para se
fazer uma demonstração sobre o quê o sistema operacional Debian pode fazer e
como usá-lo. Para se preparar um sistema, tudo o que se precisa é uma máquina
disponível e o Debian instalado nela.
</p>

<p>
Se o sistema não pode ser manipulado, apenas configure uma demonstração bacana.
Existe uma grande variedade de demonstrações no repositório Debian;
demonstrações do Mesa ou do OpenGL podem chamar a atenção. Um protetor de tela
dando créditos aos(às) desenvolvedores(as) Debian pode ser facilmente criado
pela geração de um arquivo texto com todos(as) os(as) participantes do projeto
com: <em>grep-available -n -s Maintainer ''  | sort -u</em>; e então use os
protetores de tela <q>FlipText</q>, <q>Fontglide</q>, <q>Noseguy</q>,
<q>Phosphor</q> ou <q>Starwars</q>.
Alternativamente, você pode deixar que as pessoas naveguem na Internet usando um
computador com Debian.
</p>

<p>
Você também pode preparar uma demonstração
<a href="https://wiki.debian.org/DebianInstaller/BabelBox">BabelBox</a>, que é
um pouco mais complexa, mas bem impressionante. A demonstração Babelbox,
desenvolvida pelo Christian Perrier, é um sistema que se aproveita das
capacidades de automação do instalador Debian para, automaticamente, instalar o
Debian em um sistema (opcionalmente) usando todos os diferentes idiomas para os
quais o instalador foi traduzido, um de cada vez. Por favor, leia a página
<a href="https://wiki.debian.org/DebianInstaller/BabelBox">wiki</a> para mais
detalhes de como o sistema precisa ser configurado (você vai precisar de duas
máquinas diferentes).
</p>

<p>
Se há a necessidade de hardware, nós podemos enviar uma
<a href="https://wiki.debian.org/Teams/Events/DebianEventsBox">caixa Debian de eventos</a>
para você.
</p>

<toc-add-entry name="stickers">Adesivos</toc-add-entry>

<p>
Uma das coisas que as pessoas mais apreciam em um estande são os adesivos.
Os adesivos do Debian são bem interessantes porque podem ser usados
para mostrar seu SO favorito nos laptops, monitores ou servidores (nota:
adesivos com fundo transparente são bem mais legais que aqueles com fundo
branco).
</p>

<p>
Você tanto pode usar os <a href="$(HOME)/logos/">logotipos</a> (sob as condições
expressas) ou quaisquer outros <a href="materials/stickers/">adesivos</a>
disponíveis. No subdiretório <a href="materials/stickers/pages/">pages</a> você
encontrará a fonte para a criação de uma página cheia de logotipos que podem ser
úteis para impressão em impressoras normais (coloridas), em papel adesivo.
</p>

<p>
Adicionalmente, a <a href="https://www.fsf.org/">Free Software Foundation</a>
tem alguns adesivos legais dizendo <em>GNU protected</em> (protegido pelo GNU),
<em>GNU &amp; Linux &ndash; the dynamic duo</em> (GNU &ndash; Linux a dupla
dinâmica) e <em>GNU/Linux inside</em>; todos(as) deveriam tê-los, sério.
</p>


<toc-add-entry name="foilstickers">Adesivos metalizados</toc-add-entry>

<p>
Uma coisa legal de usar em um estande são os adesivos. Mas a maioria deles
é impressa e geralmente estraga com a luz do sol, água ou sujeira. Um adesivo
metalizado é uma lâmina autocolante que pode ser cortada em certos formatos
usando-se um tipo especial de plotadora. Desse modo não há fundo branco,
transparente ou qualquer outro &ndash; somente a lâmina metalizada. Eles são
ótimos para serem colocados em laptops, carros, nos computadores do estande, nos
estandes em geral e outros lugares.
</p>

<p>
Outra vantagem é a variabilidade da escala. Os tamanhos possíveis giram em torno
de 3&nbsp;cm até 1,20&nbsp;m, variando de acordo com o tamanho do plotadora.
</p>

<p>
Os adesivos podem ser usados para melhorar a aparência do estande; você também
pode usar adesivos menores para dar aos(às) visitantes e para outras ações.
</p>

<p>
Adesivos são baratos de se produzir, mas o valor em dinheiro é alto, então pode
ser uma boa vendê-los ou repassá-los para doações.
</p>

<p>
Muitas conferências Linux e exposições, incluindo a DebConf, tem mostrado
que os(as) visitantes gostam bastante dos adesivos e estão dispostos(as) a fazer
doações.
</p>

<p>
Você pode conseguir mais informações, amostras e arquivos no
<a href="materials/stickers/">diretório de adesivos</a>. Por exemplo, você pode
imprimir uma única página com diferentes adesivos laminados. Para isto, você
pode usar a
<a href="materials/stickers/pages">impressão com vários adesivos em uma página</a>.
</p>


<toc-add-entry name="cards">Cartão de visitas do Debian para desenvolvedores(as)</toc-add-entry>

<p>
Já que as pessoas (desenvolvedores(as) ou não) dedicam bastante tempo para o
Debian em seus momentos livres, é legal preparar um presente para
eles(as). Um cartão de visitas do Debian é uma coisa bacana para se mostrar e
faz com que a pessoa que o possui fique orgulhosa!
</p>

<p>
Além disso, pessoas que estão à frente da organização de eventos e
estandes, e que entram em contato com muitos líderes e coordenadores(as) de
projetos durante os eventos sociais, podem trocar seus cartões de visitas.
Dar cartões de visitas com o logotipo do Debian contribui para a imagem
do Debian e é útil para comunicar informações importantes que usualmente
estariam escritas em um papel (como um endereço de e-mail ou sua fingerprint
GPG...).
</p>

<p>
Existe um <a href="materials/business-cards/">protótipo</a> para cartões de
visitas que você pode usar. É fácil personalizá-lo já que você só precisa
editar o arquivo <tt>card.tex</tt> com seus dados pessoais e rodar
<tt>latex card.tex</tt>. Veja o
<a href="materials/business-cards/traditional/card.ps.gz">resultado</a>.
</p>


<toc-add-entry name="slides">Slides</toc-add-entry>

<p>
Se você precisa, urgentemente, fazer uma palestra sobre &ldquo;O que é o Projeto
Debian?&rdquo;, não se preocupe. Você pode <a href="materials/slides/debian_mgp_slides.tar.gz">\
pegar</a> os slides ou <a href="materials/slides/">navegar pelos fontes</a> em
busca de ideias. Você também pode dar uma olhada na <a href="https://wiki.debian.org/Presentations">lista de palestras</a>
dadas por diversas pessoas, que cobrem o Debian ou outros aspectos do projeto.
O Joey também preparou uma
<a href="https://www.infodrom.org/Debian/events/talk-general.html">estrutura</a>
genérica para uma apresentação voltada ao Debian que você pode usar como base
para sua palestra.
</p>

<p>
Existem ainda umas imagens de fundo legais e disponíveis que você pode usar
para melhorar seus slides; por exemplo, este
<a href="http://themes.freshmeat.net/projects/debian_gel_logo/">logotipo Debian Gel</a>
chique do Patrick McFarland. Se você souber de mais imagens adequadas,
<a href="mailto:events@debian.org">nos avise</a>.
</p>


<toc-add-entry name="tshirts">Camisetas</toc-add-entry>

<p>
Camisetas são meio caras para se fazer, mas são ótimos presentes para pessoas
que ajudam no estande ou na organização do evento (se elas realmente ajudarem).
Você pode dar uma olhada em
<a href="materials/tshirts/">alguns gráficos</a> usados para camisetas.
</p>

<p>
Se quiser converter seu estande em uma empresa de venda de merchandise, você pode
(se o evento permitir) tentar vender camisetas. Existe uma quantidade de lojas
na <a href="$(HOME)/events/merchandise">página de merchandise</a> com quem você
pode cooperar. Se precisar de mais ideias, pode perguntar na
<a href="eventsmailinglists">lista de discussão dos eventos relevantes</a> ou,
se isto não for frutífero, mande um e-mail em inglês para
<a href="mailto:events@debian.org">events@debian.org</a>.
</p>


<toc-add-entry name="cdroms">CD-ROMS</toc-add-entry>

<p>
Se você comprar CDs virgens com o objetivo de entregar CD-ROMs, você pode querer
gastar um pouco mais de dinheiro e, ao queimar os CDs, não esqueça de fazê-los
parecer mais bonitos que um CD-ROM em branco (use uma arte decorativa).
A principal fonte de informações para CD-ROMs do Debian estão nas
<a href="$(HOME)/CD/">páginas de CD do Debian</a>, que detalham o processo de
download. Elas também incluem <a href="$(HOME)/CD/artwork/">artes decorativas</a>
bacanas para usar como capas e etiquetas. Por favor, tome todas as precauções
para que as ações estejam em <a href="$(HOME)/CD/vendors/legal">conformidade</a>
com as respectivas licenças do software.
</p>


<toc-add-entry name="other">Outras fontes</toc-add-entry>

<p>
Uma boa fonte de ideias e imagens para uso no merchandising do Debian
é <a href="http://gnuart.onshore.com/">http://gnuart.onshore.com/</a>.
</p>

<p>
<Strong>Nota:</Strong> Alguma das fontes (como folhetos, panfletos ou slides)
podem ter se tornado obsoletas desde que foram preparadas. Se for utilizá-las
para um novo evento, certifique-se de que a informação esteja atualizada, ou
retifique-as se necessário. Se o fizer, por favor, envie as mudanças através de
um bug para debian-www ou por e-mail (em inglês) para
<a href="mailto:events@debian.org">events@debian.org</a>.
</p>


<toc-add-entry name="credits">Créditos</toc-add-entry>

<p>
O material aqui descrito e disponibilizado por links é produto de contribuições
de pessoas que devem ser agradecidas, já que deixaram mais fácil a vida de
outras pessoas. Seus agradecimentos devem ir para
</p>

<ul>
<li>Anand Kumria pelos pôsteres
    (<a href="https://web.archive.org/web/20080515224646/http://people.debian.org/~akumria/posters/">originalmente aqui</a>).</li>
<li>Frank Neumann pelos folhetos e slides.</li>
<li>Javier Viñuales pelos adesivos disponibilizados numa página única.</li>
<li>Jason Gunthorpe pelos cartões de visitas.</li>
<li>Jim Westveer contribuiu com o design de camisetas e etiquetas de CD
    (<a href="https://web.archive.org/web/20080515224601/http://people.debian.org/~jwest/graphics/">originalmente aqui</a>).</li>
<li>Wichert Akkerman pelos folhetos e slides.</li>
<li>Bdale Garbee pelos folhetos e slides.</li>
<li>Thimo Neubauer pelo mapa-múndi.</li>
<li>Dennis Stampfer pelos adesivos metalizados.</li>
</ul>
