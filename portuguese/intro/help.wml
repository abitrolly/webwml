#use wml::debian::template title="Como você pode ajudar o Debian?"
#use wml::debian::translation-check translation="ec0e5f6417c9c57e993abde3ee192fd74dfd76b7"

### TRADUTORES: A Revisão 1.27 adiciona algumas palavras para o item de
### submissão do popcon, e a Revisão 1.28 é a reorganização dos itens (não há
### mudanças na descrição de cada item, somente foram adicionadas seções de
### título e as listas não numeradas por seção. Veja #872667 para detalhes e
### patches.

<p>Se você está considerando ajudar no desenvolvimento do Debian
existem várias áreas nas quais tanto os(as) usuários(as) experientes como os(as)
inexperientes podem ajudar:</p>

# TBD ("a ser feito") - Descrever os requisitos por tarefa?
# por exemplo: conhecimento da língua inglesa, experiência na área X, etc...

<h3>Programação</h3>

<ul>
<li>Você pode empacotar os aplicativos que você tem experiência e considera
importantes para o Debian, tornando-se o(a) mantenedor(a) desses pacotes. Para
mais informações, leia o <a href="$(HOME)/devel/">Canto dos(as) desenvolvedores(as)
Debian</a>.</li>
<li>Você pode ajudar a <a href="https://security-tracker.debian.org/tracker/data/report">rastrear</a>,
<a href="$(HOME)/security/audit/">encontrar</a> e
<a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">corrigir</a>
<a href="$(HOME)/security/">problemas de segurança</a> dentro dos pacotes no Debian.
Você também pode ajudar a fortalecer
<a href="https://wiki.debian.org/Hardening">pacotes</a>,
<a href="https://wiki.debian.org/Hardening/RepoAndImages">repositórios e imagens,</a>
e <a href="https://wiki.debian.org/Hardening/Goals">outras coisas</a>.
</li>
<li>Você pode ajudar a manter aplicações que já estão disponíveis no sistema
operacional Debian, especialmente aquelas aplicações que você usa muito e
conhece bem, contribuindo com correções (patches) ou informações adicionais no
<a href="https://bugs.debian.org/">Sistema de Rastreamento de Bugs</a> para
esses pacotes. Você também pode se envolver diretamente na manutenção de
pacotes tornando-se um(a) membro(a) de um time de manutenção, ou pode se envolver com
o software que está sendo desenvolvido para o Debian, juntando-se a um projeto
de software no <a href="https://salsa.debian.org/">Salsa</a>.</li>
<li>Você pode ajudar a portar o Debian para alguma arquitetura que você tem
experiência, seja iniciando um novo porte ou contribuindo para portes
existentes. Para mais informações, veja a
<a href="$(HOME)/ports/">lista de portes disponíveis</a>.</li>
<li>Você pode ajudar a melhorar os serviços
<a href="https://wiki.debian.org/Services">existentes</a> relacionados ao
Debian ou criar novos serviços que sejam
<a href="https://wiki.debian.org/Services#wishlist">necessários</a> pela
comunidade.
</ul>

<h3>Testes</h3>

<ul>
<li>Você pode simplesmente testar o sistema operacional e os programas
fornecidos nele, e reportar qualquer erro ainda não conhecido ou bugs que
você encontrar, utilizando o
<a href="https://bugs.debian.org/">Sistema de Rastreamento de Bugs</a>. Tente
também navegar nos bugs associados aos pacotes que você usa e forneça mais
informações se você puder reproduzir os problemas descritos.</li>
<li>Você pode ajudar com o
<a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">teste do
instalador e imagens ISO live</a>.
</ul>

<h3>Suporte ao(a_ usuário(a)</h3>
<ul>
# Tradutores, faça um link diretamente para o lista de discussão de tradução e
# providencie um link para o canal de IRC na língua do usuário
<li>Se você é um(a) usuário(a) experiente, você pode ajudar outros(as) usuários(as) através da
<a href="$(HOME)/support#mail_lists">lista de discussão de usuários(as)</a>, e mais
especificamente através da lista de discussão de usuários(as) que falam português
<a href="https://lists.debian.org/debian-user-portuguese">debian-user-portuguese</a>,
ou através do canal do IRC <tt>#debian-br</tt>. Para mais informações sobre opções
de suporte e fontes disponíveis, leia as
<a href="$(HOME)/support">páginas de suporte</a>.</li>
</ul>

<h3>Tradução</h3>
<ul>
# TBD ("para ser feito") - link para as listas de discussão de tradutores
# Tradutores, crie um link diretamente para as páginas do seu grupo
<li>Você pode ajudar traduzindo aplicações ou informações relacionadas ao Debian
(páginas web, documentação) para o seu idioma, envolvendo-se em um projeto de tradução.
As discussões geralmente são tratadas através da
<a href="https://lists.debian.org/debian-i18n/">lista de discussão i18n</a>,
e o projeto de tradução para português é coordenado na
<a href="https://lists.debian.org/debian-l10n-portuguese/">lista de discussão l10n-portuguese</a>.
Você encontrará mais informações do projeto de tradução do Debian para o português nas
<a href="https://www.debian.org/international/Portuguese">páginas do projeto</a>
Você pode até iniciar um novo grupo de internacionalização se não houver nenhum
para seu idioma. Para mais informações, leia as
<a href="$(HOME)/international/">páginas de internacionalização</a>.</li>
</ul>

<h3>Documentação</h3>
<ul>
<li>Você pode ajudar escrevendo documentação, seja trabalhando com a
documentação oficial fornecida pelo <a href="$(HOME)/doc/ddp">Projeto de
Documentação do Debian</a> ou contribuindo na
<a href="https://wiki.debian.org/">Wiki do Debian</a>.</li>
<li>Você pode etiquetar e categorizar pacotes no site web
<a href="https://debtags.debian.org/">debtags</a>
para que nossos(as) usuários(as) possam encontrar softwares mais facilmente.</li>
</ul>

<h3>Eventos</h3>
<ul>
<li>Você pode ajudar com o desenvolvimento da face <em>pública</em> do Debian e
contribuir para o <a href="$(HOME)/devel/website/">site web</a>, ou ajudar na
organização de <a href="$(HOME)/events/">eventos</a> pelo mundo.</li>
<li>Ajude o Debian a promover-se, falando do projeto e demonstrando-o para
outras pessoas.</li>
<li>Ajude a criar ou a organizar um
<a href="https://wiki.debian.org/LocalGroups">grupo Debian local</a>, com
reuniões regulares e/ou outras atividades.</li>
<li>Você pode ajudar na <a href="https://debconf.org/">Conferência Debian</a>
anual, inclusive com a
<a href="https://wiki.debconf.org/wiki/Videoteam">gravação em vídeo das
palestras</a>, <a href="https://wiki.debconf.org/wiki/FrontDesk">recebendo os(as)
participantes à medida que chegam</a>,
<a href="https://wiki.debconf.org/wiki/Talkmeister">ajudando os(as) palestrantes
antes das apresentações</a>, nos eventos especiais (como a festa de queijos e
vinhos), na montagem, na desmontagem e em outras coisas.
</li>
<li>
Você pode ajudar a organizar a <a href="https://debconf.org/">Conferência
Debian</a> anual, MiniDebConfs na sua região,
<a href="https://wiki.debian.org/DebianDay">festas do Dia Debian (Debian Day)</a>,
<a href="https://wiki.debian.org/ReleaseParty">festas de lançamento de uma nova versão (release parties) </a>,
<a href="https://wiki.debian.org/BSP">festas de caça a bugs (bug squashing parties)</a>,
<a href="https://wiki.debian.org/Sprints">sprints de desenvolvimento</a> e
<a href="https://wiki.debian.org/DebianEvents">outros eventos</a> ao redor do mundo.
</li>
<li>Você pode ver a <a href="https://wiki.debian.org/Brasil/Eventos/">lista de eventos no Brasil</a>
e participar da
<a href="https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos">lista de discussão debian-br-eventos</a>
para saber mais sobre esses eventos ou ajudar a organizá-los.</li>
</ul>

<h3>Doação</h3>
<ul>
<li>Você pode <a href="$(HOME)/donations">doar dinheiro, equipamentos e
serviços</a> para o projeto Debian, para que tanto usuários(as) quanto
desenvolvedores(as) possam ser beneficiados(as). Nós estamos em constante procura por
<a href="$(HOME)/mirror/">espelhos espalhados pelo mundo</a> nos quais nossos(as)
usuários(as) possam confiar, e por <a href="$(HOME)/devel/buildd/">sistemas de
auto-builder</a> para nossos portes.</li>
</ul>

<h3>Use Debian!</h3>
<ul>
<li>
Você pode <a href="https://wiki.debian.org/ScreenShots">fazer capturas de tela</a>
dos pacotes e o <a href="https://screenshots.debian.net/upload">upload</a>
das telas para
<a href="https://screenshots.debian.net/">screenshots.debian.net</a>, para que
nossos(as) usuários(as) possam ver como se parece um software no Debian antes de
usá-lo.</li>
<li>Você pode habilitar as
<a href="https://packages.debian.org/popularity-contest">submissões do
"popularity-contest"</a> para que nós saibamos quais pacotes são populares e
mais úteis para todos(as).</li>
</ul>

<p>Como você pode ver, há várias formas pelas quais você pode se envolver com o
projeto e somente poucas delas requerem que você seja um(a) desenvolvedor(a) Debian.
Muitos dos diferentes projetos têm mecanismos para permitir acesso direto às
árvores de código-fonte pelos(as) colaboradores(as) que tenham se mostrado confiáveis e
valiosos. Normalmente, as pessoas que acham que podem ficar mais envolvidas no
Debian <a href="$(HOME)/devel/join">se juntarão ao projeto</a>, mas isso nem
sempre é necessário.</p>

<h3>Organizações</h3>

<p>Sua organização educacional, comercial, sem fins lucrativos ou governamental
pode ter interesse em ajudar o Debian.
Sua organização pode
<a href="https://www.debian.org/donations">fazer uma doação</a>,
<a href="https://www.debian.org/partners/">formar uma parceria de longo prazo</a>,
<a href="https://www.debconf.org/sponsors/">patrocinar nossas conferências</a>,
<a href="https://wiki.debian.org/MemberBenefits">prover produtos ou serviços
gratuitos para contribuidores(as) Debian</a>,
<a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">prover
hospedagem gratuita para serviços experimentais Debian</a>,
manter espelhos para nossos
<a href="https://www.debian.org/mirror/ftpmirror">softwares</a>,
<a href="https://www.debian.org/CD/mirroring/">mídias de instalação</a>
ou <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">vídeos de conferências</a>,
ou promover nossos softwares e nossa comunidade
<a href="https://www.debian.org/users/">fazendo uma recomendação</a>
ou vendendo <a href="https://www.debian.org/events/merchandise">materiais publicitários</a>,
<a href="https://www.debian.org/CD/vendors/">mídias de instalação</a>,
<a href="https://www.debian.org/distrib/pre-installed">sistemas pré-instalados</a>,
<a href="https://www.debian.org/consultants/">consultoria</a> ou
<a href="https://wiki.debian.org/DebianHosting">hospedagem</a>.
</p>

<p>
Você também pode ajudar ao encorajar seus(suas) funcionários(as) a participar
da nossa comunidade, apresentando o Debian através da utilização de nosso
sistema operacional na sua organização, explicando sobre o sistema
operacional Debian e a comunidade, direcionando-os(as) para que contribuam
durante o expediente de trabalho ou enviando-os(as) para nossos
<a href="$(HOME)/events/">eventos</a>.
</p>

# <p>Links relacionados:
